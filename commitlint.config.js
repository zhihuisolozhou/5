
//提交  连接的配置
module.exports = {
    extends: ["@commitlint/config-conventional"],
    rules: {//指定拦截规则
        "type-enum": [
            "always",
            [
                'build',
                'chore',
                'ci',
                'docs',
                'feat',
                'fix',
                'perf',
                'refactor',
                'revert',
                'style',
                'test'
            ]
        ],
        "type-case": [0],
        "type-empty":[0],
        "subject-case":[0,'never']
    }
}