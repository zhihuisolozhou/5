const Koa = require("koa");
const Router = require("@koa/router");

const app = new Koa();
const router = new Router();

const AlipaySDK = require("alipay-sdk").default;
const AlipayFormData = require("alipay-sdk/lib/form").default;

const alipaySdk = new AlipaySDK({
  appId: "2021000121670852", // 开放平台上创建应用时生成的 appId
  signType: "RSA2", // 签名算法,默认 RSA2
  gateway: "https://openapi.alipaydev.com/gateway.do", // 支付宝网关地址 ，沙箱环境下使用时需要修改
  alipayPublicKey:
    "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAwz726ZM5/XsqNvRjhp4CcAzPYIsx//n2aXwnQaucRDpZev0e+RvZZr48n0LB9OJ9JFC93FdvruMKrphYan20R7OrYWQ5vIqEOyT9TwSzvbGuOUeuG8FSWCZRrPKwXyduKUR0zd+F2Grf+6ecnF3EoZE4CMC0tpmp4ZnvVfrGS+CQEtUT0SRCIpRv7POjKvOgHdXeu3utvV8UYBBcDxogWrQXu0BaFJMpw1pcQYIVO9m1GH+0ETA5+rmJ9Z/WDxzSqvxFni72Vx3+dN5t4VWjH9wiNZAQdUUNM3yjqy8rVuKlh2b1L9Ui+lG2m3QcXjVENXvNFwscdqIYCkzXmfNkPQIDAQAB", // 支付宝公钥，需要对结果验签时候必填
  privateKey:
    "MIIEpAIBAAKCAQEAjxAXAX3r3Z6aCoyrrTquuiJKPPxNJ23IeqNiPwAZzScVUdEtyhEWbhDhNHa6eJMYMVdhCdh7kCo/b8lWrWXFjq3qoEudgnHycZOSKnOOnIUnnzbdJXHbRvUvnY0RPM4shQH7bzNPph5Z46bMroybX71gZ7qxkfZF6+fWA7ztB680DyPbdQHsxorHUzSUkRMh4EWrLgrGBWfkGigg3PAS2UBxtc/z83DkxAwP8MftWikR2+QeAyGcw5buzQorgkArqHtaydcXJGIUgEIPJu0aZTrg7Cdpep2W1OE34RhHxgb6NU1hz9x6D5WKr9QuZiWtRbZcO5L0P1+WpkE7qJZR2QIDAQABAoIBAEJcdE982awzpcC+/+ObFPHCdi1T23YutpAsuaX/pfhD6N/K6DKTOpRTwCNV76l+kfpRTfIDOU3mSwbPniVQ0BMfGXbBnI6lzJBio6qNb9mq6nYwPOThq5V+pleu2BAckwofoUIzyv6Dla7D8K4HCwueDNoQ/811L3n+UIfrWwf0xRC443iMWwzMS2QJXwN8PnFIbHV3+DEUHLWCGb+/Y/aGCo6A+sC8yQs9NM6R4VBCmUJdZsniaCToEtifxWea3nqyjiWWuDVv4wLlXPSPtZnh4q9Mn0bgvjqgQb9qM7S9ZMZdPGhUqfvdEtckDqYgRi5grZjk54rckf+5UJ50XwECgYEA/Fc2cbVGz7voVeAtYgQYUeSJeOSHP7ogJ8GC31QokddErB9HGAGfL2eEkB3SC67lurw1AeMV0x2MmzHIcKJHqlgeBjz+PQ/YYVV+RbHDTdixwhTfCUF1VPOSyGdNG7d0f37gVqbDZYjnAfUuufvWOa34GtkqcVsa61ZX+ZwmhrcCgYEAkSMx9XV+DB9SeapdZ0JgHklJJnK0uSX9PrDGANhU8kWJgYH1HBCcRTFcYhUx1yQ5SiWw60oBqlP72C6AiLDPfnAE3lFAnieaM4w+ZhKy4CPINMsHCl3ONcrz7D6to3FTNSggfV7ZVIWLYPI8CrIiWIWhdKN3DyeaHmibgw+g2+8CgYEA0oOnhgEnTBKFZ28YLVchOuCf110/f+i0/rWvisTY+6JNRhLp6psfxBpmOxzFqL2GJdzxk9twz6ktLiPABkc75l6Q3b9wklxvtfPu3hHIz17iHPnwf/Y9/lsdr1aOu3q3E84zNdb11/T4U2fKTZpNQ91gyHU3tU2+DWsBBdpBQc0CgYB2iuCLM2O4UvzyEowngrcvBXyRwq6JatXlFk0i846uMs2U0ah5O7fna+Gg2t7Sceu4+//8rX2euOnCMHkbLA7NnoDbdFHq2z2/TJTKmgsvtwoW1JzzgpqipRmFo8v+6tijWhrvS0EHH+wvSqpvsf8LflnZAoqD/jyLCbW63lDsUQKBgQDxlC4jZvigYhKN6tMc8oPn3JgiqbhVocwH5MUw4j3uYrWyihbKWx+UbrnIXYcbr1yUqKMuXIQGvNgNeC6shmcjDcSrrNOZwOaHhkp6gjrz3sIiNnTqLe87x5Ew0Fv5X+a3QyPUj/L2JVHScHd7KR51B4KpWevcF7Wkuzwmir+HfA==", // 应用私钥字符串
});

router.get("/api/pay", async (ctx, next) => {
  // let { item } = ctx.query;
  // console.log(item, "ieieieieiei");
  let { id, pay, title } = ctx.query;
  console.log(id);
  const formData = new AlipayFormData();
  formData.setMethod("get");
  formData.addField("notifyUrl", `http://10.37.11.2:3000/detail/${id}`);
  formData.addField("bizContent", {
    outTradeNo: `${id}`, // 商户订单号,64个字符以内、可包含字母、数字、下划线,且不能重复
    productCode: "FAST_INSTANT_TRADE_PAY", // 销售产品码，与支付宝签约的产品码名称,仅支持FAST_INSTANT_TRADE_PAY
    totalAmount: `${pay}`, // 订单总金额，单位为元，精确到小数点后两位
    subject: `${title}`, // 订单标题
    body: "商品详情", // 订单描述
  });
  formData.addField("returnUrl", `http://10.37.11.2:3000/detail/${id}`);
  const result = await alipaySdk.exec(
    // result 为可以跳转到支付链接的 url
    "alipay.trade.page.pay", // 统一收单下单并支付页面接口
    {}, // api 请求的参数（包含“公共请求参数”和“业务参数”）
    { formData: formData }
  );

  ctx.body = {
    code: 200,
    msg: "支付获取成功",
    result: result,
  };
  console.log(result, "rerererererer");
});

app.use(router.routes());
app.listen(9000, () => {
  console.log("服务器启动成功 http://localhost:9000");
});
