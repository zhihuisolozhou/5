import React, { useEffect, useState } from "react";
import QRCode from "qrcode";
//检测图片是否加载成功
const ImageUrl = (url: any) => {
  return new Promise((resolve, reject) => {
    const img = new Image();
    //允许跨域
    img.crossOrigin = "Anonymous";
    img.onload = () => {
      resolve(img);
    };
    img.onerror = error => {
      reject(error);
    };
    img.src = url;
  });
};
//绘制
const drawURL = async (ctx: any, data: any, canvasDom: any) => {
  //填充背景
  ctx.fillStyle = "#fff";
  //绘制矩形  画布
  ctx.fillRect(0, 0, 840, 500);
  //标题
  ctx.font = "20px hotpink";
  ctx.fillStyle = "#000";

  //绘制填充的文本
  ctx.fillText(data.title, 130, 310);
  ctx.fillText("原图分享自ikun", 130, 390);
  //绘制标题下面的线
  ctx.beginPath(); //绘制路径开始
  ctx.strokeStyle = "#fff"; // 填充色
  ctx.moveTo(0, 40);
  ctx.lineTo(400, 40);
  //开始绘制
  ctx.stroke();
  //绘制图片
  const img = await ImageUrl(data.cover);
  const imgss = await ImageUrl(window.localStorage.getItem("img"));
  console.log(imgss, "--------------------------------------------");

  ctx.drawImage(img, 0, 50, 400, 200);
  ctx.drawImage(imgss, 0, 280, 130, 130);
  //绘制下面的文字
  ctx.font = "20px #000";
  ctx.fillStyle = "#000";

  //生成url把canvas转为图片

  const imageurl = canvasDom.current.toDataURL();

  return imageurl;
};
//生成 canvas
const creatcanvas = async (data: any, canvasDom: any) => {
  canvasDom.current.width = 400;
  canvasDom.current.height = 500;
  //创建上下文
  const ctx = canvasDom.current.getContext("2d"); //获取canvas对象 2D绘图的上下文
  const image = await drawURL(ctx, data, canvasDom);
  return image;
};
const useCanvas = (data: any = {}, canvasDom: any) => {
  const [url, setUrl] = useState("");
  const [imgs, setImgs] = useState("");

  //二维码
  useEffect(() => {
    QRCode.toDataURL(data.title, function (err, url) {
      setImgs(url);
    });
  }, [data]);
  window.localStorage.setItem("img", imgs);

  //二维码
  useEffect(() => {
    QRCode.toDataURL(data.title, function (err, url) {
      setImgs(url);
    });
  }, [data]);
  window.localStorage.setItem("img", imgs);

  //自执行函数
  useEffect(() => {
    (async () => {
      console.log(
        "----------------------------------------------------------------start-------------------"
      );
      let url = await creatcanvas(data, canvasDom);
      console.log(
        url,
        "-------------------------------------------------end-----------------------"
      );
      setUrl(url as string);
    })();
  }, [data]);

  return url;
};

export default useCanvas;
