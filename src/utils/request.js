import axios from "axios"

const request = axios.create({
  baseURL: "https://creationapi.shbwyz.com",
})

request.interceptors.request.use(function (config) {
  // 在发送请求之前做些什么
  return config;
}, function (error) {
  // 对请求错误做些什么
  return Promise.reject(error);
});

// 添加响应拦截器
request.interceptors.response.use(function (response) {
  response.config["Access-Control-Allow-Origin"] = "*";
  // 对响应数据做点什么
  return response;
}, function (error) {
  // 对响应错误做点什么
  return Promise.reject(error);
});

export default request