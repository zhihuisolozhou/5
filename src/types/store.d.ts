import { type } from "os";

export interface ALLSTATE {
  [x: string]: any;
  swiperlist: STATELISTTYPE[];
  talkslist: STATELISTTYPE[];
  commentlist: STATELISTTYPE[];
  copycommentlist: STATELISTTYPE[];
}

export interface STATELISTTYPE {
  cover: string;
  title: string;
  views: number;
  likes: number;
  updateAt: string;
  isCommentable: boolean;
  id: string;
  isRecommended: boolean;
}

export interface SWIPEREDLIST {
  cover: string;
  type: "SWIPEREDLIST";
  paylaod: SWIPEREDLIST[];
}

export interface SETLIKES {
  id: string;
  type: "SETLIKES";
  paylaod: string;
}

export interface SETVIEWS {
  id: string;
  views: number;
  type: "SETVIEWS";
  paylaod: STATELISTTYPE;
}

export interface TALKINGLIST {
  id: string;
  type: "TALKINGLIST";
  paylaod: string;
}

export interface ADDTALKS {
  textValue: string;
  type: "ADDTALKS";
  paylaod: string;
}

export interface ADDCOMMENT {
  textValue: string;
  type: "ADDCOMMENT";
  paylaod: string;
}

export interface COMMENTLIST {
  type: "COMMENTLIST";
  paylaod: STATELISTTYPE[];
}

export type ALLACTION =
  | STATELISTTYPE
  | SWIPEREDLIST
  | SETLIKES
  | SETVIEWS
  | TALKINGLIST
  | ADDTALKS
  | COMMENTLIST
  | ADDCOMMENT;
