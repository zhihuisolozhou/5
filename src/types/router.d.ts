export interface ROUTERTYPE {
  path: string;
  to?: string;
  element?: any;
  children?: ROUTERTYPE[];
}
