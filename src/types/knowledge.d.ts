import { type } from "os";

export type DISPATCH = typeof store.dispatch;

export interface DATATYPE {
  knowStoreList: KNOWLISTTYPE[];
  isModalOpen: boolean;
  KnowShare_item: KNOWLISTTYPE[];
  Know_detail_right_list: KNOWLISTTYPE[];
  detail_right_detail_list: KNOWLISTTYPE[];
  small_detail: any;
  small_detail_children: any;
  detail_children_reading: DETAILCHILDREN[];
}

export interface DETAILCHILDREN {
  children: DETAILCHILDREN[];
  length: 0;
  content: null;
  cover: string;
  createAt: "2022-09-17T04:01:37.616Z";
  html: null;
  id: "921bb318-8588-47c3-b83f-0d5427f077b4";
  isCommentable: boolean;
  likes: 0;
  order: 0;
  parentId: null;
  publishAt: "2022-09-17T04:01:37.000Z";
  status: string;
  summary: string;
  title: string;
  toc: null;
  updateAt: "2022-09-19T11:09:29.000Z";
  views: number;
}

export interface KNOWLISTTYPE {
  id: "string";
  parentId: "string";
  order: 0;
  title: "string";
  cover: "string";
  summary: "string";
  content: "string";
  html: "string";
  toc: "string";
  status: "string";
  views: 0;
  likes: 0;
  isCommentable: true;
  publishAt: "2022-09-15T06:59:30.877Z";
  createAt: "2022-09-15T06:59:30.878Z";
  updateAt: "2022-09-15T06:59:30.878Z";
}

export const KNOWTYPE = "setknow";
export type KNOWTYPE = typeof KNOWTYPE;
export interface KNOWTYPEACTION {
  type: KNOWTYPE;
  payload: KNOWLISTTYPE[];
}

export const KNOWSHARETYPE = "setknow_share";
export type KNOWSHARETYPE = typeof KNOWSHARETYPE;
export interface KNOWSHARETYPEACTION {
  type: KNOWSHARETYPE;
  payload: boolean;
}

export const KNOWSHAREIDTYPA = "setknow_share_item";
export type KNOWSHAREIDTYPA = typeof KNOWSHAREIDTYPA;
export interface KNOWSHAREIDTYPACTION {
  type: KNOWSHAREIDTYPA;
  payload: KNOWLISTTYPE;
}

export const KNOWDETAILRIGHT = "set_detail_right_list";
export type KNOWDETAILRIGHT = typeof KNOWDETAILRIGHT;
export interface KNOWDETAILRIGHTACTION {
  type: KNOWDETAILRIGHT;
  payload: KNOWLISTTYPE;
}

export const SMALLDETAILCHILDREN = "set_small_detail_children";
export type SMALLDETAILCHILDREN = typeof SMALLDETAILCHILDREN;
export interface SMALLDETAILCHILDRENACTION {
  type: SMALLDETAILCHILDREN;
  payload: KNOWLISTTYPE;
}

export const DETAILREADING = "set_detail_children_reading";
export type DETAILREADING = typeof DETAILREADING;
export interface DETAILREADINGACTION {
  type: DETAILREADING;
  payload: KNOWLISTTYPE;
}

export type ALLACTION =
  | KNOWTYPEACTION
  | DETAILREADINGACTION
  | KNOWSHARETYPEACTION
  | KNOWSHAREIDTYPACTION
  | KNOWDETAILRIGHTACTION
  | SMALLDETAILCHILDRENACTION;
