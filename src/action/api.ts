import axios from "../page/home/index/archives/type/request";
import { Dispatch } from "redux";

export const get_data = () => {
  return async (dispatch: Dispatch) => {
    await axios.get("/api/article").then(res => {
      dispatch({
        type: "GET_DATA",
        payload: res.data.data[0],
      });
    });
  };
};
export const search_data = (val: string) => {
  return async (dispatch: Dispatch) => {
    dispatch({
      type: "SEARCH_DATA",
      payload: val,
    });
  };
};
