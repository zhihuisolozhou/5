import axios from '../utils/request';
import { Dispatch } from 'redux'
import { DATATYPE,DISPATCH, KNOWTYPE, KNOWLISTTYPE, KNOWSHARETYPE, KNOWSHAREIDTYPA, KNOWDETAILRIGHT } from "../types/knowledge.d"

export const get_knowledge_data = () => {
    return async (dispatch: Dispatch) => {
        await axios.get('/api/knowledge').then(res => {
            console.log(res.data.data[0],111111111111)
            dispatch({
                type: KNOWTYPE,
                payload: res.data.data[0]
            })
        })
    }
}