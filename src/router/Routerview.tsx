import React, { Suspense } from 'react'
import {
    BrowserRouter,
    Route,
    Routes,
    Navigate
} from 'react-router-dom'
import { ROUTERTYPE } from '../types/router.d'
import routes from './Routerconfig'

function Routerview() {
    const renderouter = (routes: ROUTERTYPE[]) => {
        return routes.map((item, index) => {
            return <Route key={index} path={item.path} element={item.to ? <Navigate to={item.to}></Navigate> : <item.element></item.element>}>
                {
                    item.children && renderouter(item.children)
                }
            </Route>
        })
    }
    return (
        <Suspense>
            <BrowserRouter>
                <Routes>
                    {
                        renderouter(routes)
                    }
                </Routes>
            </BrowserRouter>
        </Suspense>
    )
}

export default Routerview