import { lazy } from "react";
export const second_level_router = [
  {
    path: "/home/article",
    element: lazy(() => import("../page/home/index/article/Article")),
    title: "article",
    key: 1,
  },
  {
    path: "/home/Archives",
    element: lazy(() => import("../page/home/index/archives/Archives")),
    title: "Archives",
    key: 2,
  },
  {
    path: "/home/KnowledgeBooks",
    element: lazy(
      () => import("../page/home/index/knowledgeBooks/KnowledgeBooks")
    ),
    title: "KnowledgeBooks",
  },
];
const routes = [
  {
    path: "/home",
    element: lazy(() => import("../page/home/Home")),
    children: second_level_router,
  },
  {
    path: "/detail/:id",
    element: lazy(() => import("../page/detail/Detail")),
  },
  {
    path: "/home/article/category/:id",
    element: lazy(() => import("../page/category/category")),
  },
  {
    path: "/home/article/DataChildren/:id",
    element: lazy(() => import("../componetns/Datachildren")),
  },
  {
    path: "/",
    to: "/home/article",
  },
  {
    path: "/home/knowledgeBooks/:id",
    element: lazy(
      () => import("../page/home/index/knowledgeBooks/knowledgeDetail2")
    ),
  },
  {
    path: "/*",
    element: lazy(() => import("../componetns/404/Logout")),
  },
  {
    path: "/home/knowledgeBooks/:id",
    element: lazy(
      () => import("../page/home/index/knowledgeBooks/knowledgeDetail2")
    ),
  },
  {
    path: "/home/knowledgeBooks/:id/:children_id",
    element: lazy(
      () => import("../page/home/index/knowledgeBooks/KnowledgeReading")
    ),
  },
];

export default routes;
