import React, { useState, useCallback, useEffect } from "react";
import { Button, Layout } from "antd";
import { Col, Row } from "antd";
import { WifiOutlined, GithubOutlined } from "@ant-design/icons";
import { second_level_router } from "../router/Routerconfig";
import { Outlet, useNavigate } from "react-router-dom";
import { NavLink } from "react-router-dom";
import RecommendToReading from "./RecommendToReading";
import "./components/Appbox.scss";
import Headers from "./header/Headers";
import style from "../styles/style.module.css";
import { BackTop } from "antd";
import SmallHeaders from "./header/SmallHeaders";
import {
  Appbox_setistitle,
  Appbox_setdatas,
  Appbox_setdatase,
} from "../page/category/aps";
//判断当前时间
const item = second_level_router.map((item, index) => {
  const key = index + 1;
  const nav = item.title ? <NavLink to={item.path}>{item.title}</NavLink> : "";
  return {
    key,
    label: nav,
  };
});
console.log(item, "item");
function Appbox() {
  const navigate = useNavigate();
  const [datas, setdatas] = useState([]);
  const [istitle, setistitle] = useState([]);
  const [datase, setdatase] = useState([]);
  const [width, setWidth] = useState(0);
  const urlParams = new URL(window.location.href);
  const pathname = urlParams?.pathname;
  const IStitle = async () => {
    let das = await Appbox_setistitle()
    setistitle(das.data.data)
  }
  const DAtas = async () => {
    let das = await Appbox_setdatas()
    setdatas(das.data.data.splice(0, 6))
  }
  const DAtase = async () => {
    const das = await Appbox_setdatase()
    setdatase(das.data.data)
  }
  useEffect(() => {
    IStitle();
    DAtas();
    DAtase()
  }, [])
  var moment = require('moment');
  moment.defineLocale('zh-cn', {
    relativeTime: {
      future: "%s内",
      past: "%s前",
      s: "几秒",
      m: "一分钟",
      mm: "%d分钟",
      h: "1小时",
      hh: "%d小时",
      d: "1天",
      dd: "%d天",
      M: "1月",
      MM: "%d个月",
      y: "1年",
      yy: "%d年"
    }
  })

  const resizeUpdata = (e: any) => {
    let w = e.target.innerWidth;
    setWidth(w);
  };

  useEffect(() => {
    let w = window.innerWidth;
    setWidth(w);
    window.addEventListener("resize", resizeUpdata);
    return () => {
      window.removeEventListener("resize", resizeUpdata);
    };
  }, []);
  useEffect(() => {
    if (window.localStorage.getItem("theme") === "true") {
      document.documentElement.className = "dark";
    } else if (window.localStorage.getItem("theme") === "false") {
      document.documentElement.className = "light";
    }
  });
  return (
    <div className={style.note}>
      <div className='apbox'>
        {
          width < 1300 ? <SmallHeaders /> : <Headers />
        }
        <main>
          <Row>
            <Col span={3}></Col>
            {
              width < 800 ? <Col span={18}>
                <div className='main'>
                  {/* 占位盒子 */}
                  <div children className="sup"></div>
                  <div className="op">
                    {" "}
                    <Outlet />
                  </div>
                </div>
              </Col>
                : (
                  <Col span={13}>
                    <div className="main">
                      {/* 占位盒子 */}
                      <div children className="sup"></div>
                      <div className="op">
                        {" "}
                        <Outlet />
                      </div>
                    </div>
                  </Col>
                )}

            {width > 700 ? (
              <Col span={5}>
                <div className="vice">
                  {/* 中间右上组件 */}
                  <RecommendToReading str={"recommendToReading"}>
                    {
                      datas.length ? datas.map((item: any, index: number) => {
                        return (
                          <li style={{ animation: `example1 ${(index + 1) * 0.1}s ease-out ${(index + 2) * 0.1}s backwards` }} key={index}
                            onClick={() => {
                              navigate(`/detail/${item.id}`, {
                                state: item
                              })
                            }}
                          >
                            <span className='datas_span'>{item.title} ·</span>
                            <span>&ensp;</span>
                            <span style={{ color: "#ccc", fontSize: '5px' }}>{moment(item.createAt).fromNow()}</span>
                          </li>
                        )
                      }) : ""
                    }
                  </RecommendToReading>
                  {/* 中间右下组件 */}
                  {
                    pathname != '/home/article' ?
                      <RecommendToReading str={"TagTitle"}>
                        {
                          datase.length ? datase.map((item: any, index: number) => {
                            return (
                              <p key={index} onClick={() => { navigate(`/home/article/category/${item.label}`, { state: item }) }}
                                className='Appbox_p'
                              >
                                <span>{item.label}</span>
                                <span style={{ float: 'right', marginRight: '10px' }}>共计{item.articleCount}篇文章</span>
                              </p>
                            )
                          }) : ""
                        }
                      </RecommendToReading> : <RecommendToReading str={"categoryTitle"}>
                        {
                          istitle.length ? istitle.map((item: any, index: number) => {
                            return (
                              <Button key={index} onClick={() => {
                                navigate(`/home/article/DataChildren/${item.label}`, {
                                  state: item
                                })
                              }} className='Appbox_p'>
                                <span>{item.label}</span>
                                <span > [{item.articleCount}]</span>
                              </Button>
                            )
                          }) : ""
                        }
                      </RecommendToReading>
                  }
                </div>
              </Col>
            ) : null}
            <Col span={3}></Col>
          </Row>
        </main>
        <>
          <BackTop />
        </>
        <footer>
          <div className="footerdiv">
            <a href=""></a>
            <WifiOutlined style={{ color: "#fff", fontSize: "30px" }} />
            <GithubOutlined style={{ color: "#fff", fontSize: "30px" }} />
          </div>
        </footer>
      </div>
    </div>
  );
}
export default Appbox;
