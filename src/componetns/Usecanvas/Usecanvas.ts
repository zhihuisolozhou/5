import React, { useEffect, useState } from 'react'
import QRCode from "qrcode";
const useCanvas = (data: any, canvasDom: any) => {
    console.log(data, '123');
    const [url, setUrl] = useState('')
    const [imgs, setImgs] = useState('')
    //检测图片是否加载成功
    const ImageUrl = (url: any) => {
        return new Promise((resolve, reject) => {
            const img = new Image()
            //允许跨域
            img.crossOrigin = 'anonymous';
            img.onload = () => {
                resolve(img)
            }
            img.onerror = (error) => {
                reject(error)
            }
            img.src = url;
        })
    }
    //绘制
    const drawURL = async (ctx: any, data: any, canvasDom: any) => {
        //填充背景
        ctx.fillStyle = '#fff';
        //绘制矩形
        ctx.fillRect(0, 0, 500, 700);
        //标题
        ctx.font = '30px';
        ctx.fillStyle = '#000';
        //绘制填充的文本
        ctx.fillText(data.title, 130, 30)
        //绘制标题下面的线
        ctx.beginPath();//绘制路径开始
        ctx.strokeStyle = 'pink'    // 填充色
        ctx.moveTo(0, 40)
        ctx.lineTo(400, 40)
        //开始绘制
        ctx.stroke()
        //绘制图片
        const img = await ImageUrl(data.cover)
        // const imgss = await ImageUrl(data.cover)
        console.log(img, '99999999');

        ctx.drawImage(img, 50, 100, 100, 100)
        //绘制下面的文字
        ctx.font = '20px #000';
        ctx.fillStyle = '#000';
        ctx.fillText(data.title, 50, 500)
        ctx.fillText(data.summaty, 50, 550);
        //生成url把canvas转为图片
        const imageurl = canvasDom.current.toDataURL();
        console.log(imageurl,"我是；路径");
        
        return imageurl
    }
    //生成 canvas
    const creatcanvas = async (data: any, canvasDom: any) => {
        canvasDom.current.width = 400;
        canvasDom.current.height = 500;
        //创建上下文  
        const ctx = canvasDom.current.getContext('2d') //获取canvas对象 2D绘图的上下文
        const image = await drawURL(ctx, data, canvasDom);
        return image
    }
    //二维码
    useEffect(() => {
        QRCode.toDataURL(data.title, function (err, url) {
            setImgs(url)
        })
    }, [data])
    window.localStorage.setItem('img', imgs)

    //自执行函数
    useEffect(() => {
        (async () => {
            let url = await creatcanvas(data, canvasDom)
            console.log(url,"79");
            setUrl(url as string)
        })()
    }, [data])
    return url
}

export default useCanvas;