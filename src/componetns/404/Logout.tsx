import React, { useEffect, useState } from 'react'
import Headers from "../header/Headers";
import SmallHeaders from "../header/SmallHeaders";
import imgs from "../404/404.png";
import { setWith } from 'lodash';
import "./style/style.scss";
import { NavLink } from 'react-router-dom';
function Logout() {
  const [width, setWidth] = useState(0)
  const widthUpdata = (e: any) => {
    let w = e.target.innerWidth;
    setWidth(w)
  }
  useEffect(() => {
    let w = window.innerWidth;
    setWidth(w)
    window.addEventListener("resize", widthUpdata)
    return (() => {
      window.removeEventListener("resize", widthUpdata)
    })
  }, [])
  console.log(width);

  return (
    <div
    >
      {
        width < 700 ? <SmallHeaders /> : <Headers />
      }
      <div className='img_404'> <button className='button_404'><NavLink to='/home/article'>backHome</NavLink></button>
        <img src={imgs} alt="" />

      </div>

    </div>
  )
}

export default Logout