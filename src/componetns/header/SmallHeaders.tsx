import React, { useState, useEffect } from 'react'
import { Button, Layout } from 'antd';
import { Menu } from 'antd';
import { SearchOutlined } from '@ant-design/icons';
import * as app from "../../action/api";
import { Input, Space } from 'antd';
import { useTranslation } from 'react-i18next';
import i18n from "../../I18n/index";
import { second_level_router } from "../../router/Routerconfig";
import { useNavigate } from 'react-router-dom';
import { NavLink } from 'react-router-dom';
import '../components/Appbox.css'
import { useDispatch, useSelector } from "react-redux";
import { DISPATCH, DATA } from "../../page/home/index/archives/type/stype";
import { Dropdown } from 'antd';
import { Affix } from 'antd';
import { Drawer } from 'antd';
import './css/style.css';



const { Header } = Layout;
const { Search } = Input;

const content = (
    <div>
        <p>Content</p>
        <p>Content</p>
    </div>
);

const buttonWidth = 70;
const item = second_level_router.map((item, index) => {
    const key = item.key;
    const nav = item.title === 'haha' ? null : <NavLink to={item.path}>{item.title}</NavLink>
    return {
        key,
        label: nav,
    };
})
function SmallHeaders() {
    const navigate = useNavigate()
    const dispatch: DISPATCH = useDispatch()
    const [flag, setFlag] = useState(false)
    const { t } = useTranslation()
    const search_data = useSelector((state: any) => {
        return state.redux.search_data
    })
    const onSearch = (value: string) => {
        dispatch(app.search_data(value))
    };
    const [lang, setLang] = useState(() => navigator.language)
    console.log(lang);
    const [open, setOpen] = useState(false);

    // const showDrawer = () => {
    //     setOpen(true);
    // };

    const onClose = () => {
        setOpen(false);
    };


    const menu = (
        <Menu
            items={[
                {
                    key: '1',
                    label: (
                        <div onClick={() => {
                            const newLang = lang === 'en' ? 'zh-CH' : 'en';
                            setLang(newLang)
                            //调用i18n的changelanguage方法
                            i18n.changeLanguage(newLang)
                        }}>{t('Chinese')}</div>
                    ),
                }


            ]}
        />
    );
    const [theme, setTheme] = useState(true)

    const [top, setTop] = useState(0);
    const [IsShow, setIsShow] = useState(false);
    const [Isif, setIsif] = useState(false);
    let scrollTop = 0
    let topValue = 0
    const getScollTop = () => {
        let scrollTop = 0;
        if (document?.documentElement && document?.documentElement?.scrollTop) {
            scrollTop = document?.documentElement.scrollTop;
        }
        else if (document?.body) {
            scrollTop = document?.body.scrollTop;
        }
        return scrollTop;
    }

    const bindHandleScroll = () => {
        scrollTop = getScollTop();
        if (scrollTop <= topValue) {
            // console.log('向上')
            setIsShow(false)
            setIsif(false)
        }
        else {
            // console.log('向下')
            setIsShow(true)
            setIsif(true)
        }
        setTimeout(function () { topValue = scrollTop; }, 0);
    }

    useEffect(() => {
        window.addEventListener('scroll', bindHandleScroll)
        return () => {
            window.removeEventListener('scroll', bindHandleScroll)
        }
    }, [])

    // pzl---------------------------------
    const dians = () => {

        setTheme(!theme)
        window.localStorage.setItem('theme', JSON.stringify(theme))
        console.log(theme, '当前');
        console.log('本地', window.localStorage.getItem('theme'));
        if (window.localStorage.getItem('theme') === 'true') {
            document.documentElement.className = 'dark';
        } else if (window.localStorage.getItem('theme') === 'false') {
            document.documentElement.className = 'light';
        }
    }
    useEffect(() => {
        if (window.localStorage.getItem('theme') === 'true') {
            document.documentElement.className = 'dark';
        } else if (window.localStorage.getItem('theme') === 'false') {
            document.documentElement.className = 'light';
        }
    })
    return (
        <div>
            <Affix className='aff' offsetTop={top} onChange={affixed => console.log(affixed)}>
                <Header style={{ width: '100%', height: '64px', position: 'fixed', left: '0', top: Isif ? '-64px' : '0', transition: 'top 0.2s', zIndex: '999999' }}>
                    <img onClick={() => {
                        console.log(123);
                        navigate('/home/article')
                    }} className='imgtop' src="https://bwcreation.oss-cn-beijing.aliyuncs.com/2021-09-17/u%3D2156833431%2C1671740038%26fm%3D26%26gp%3D0.jpg" />

                    <div className='rights'>
                        {/* @ts-ignore  */}

                        {/* 收起 */}
                        <>
                            <Button className='smallheaderbtn' type="primary" >
                                {/* @ts-ignore  */}
                                <svg onClick={() => { setOpen(!open); console.log(open) }} t="1663572462324" className="icon" viewBox="0 0 1403 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="10213" width="35" height="35"><path d="M189.62963 113.777778v196.683852h1061.925926V113.777778H189.62963z m0 477.601185h1061.925926v-196.683852H189.62963v196.683852zM189.62963 872.296296h1061.925926v-196.683852H189.62963V872.296296z" fill="#d4237a" p-id="10214"></path></svg>

                            </Button>
                            <Drawer title="Basic Drawer" placement="top" onClose={onClose} open={open}>
                                {/* <Menu

                                    theme="dark"
                                    mode="horizontal"
                                    defaultSelectedKeys={['1']}
                                    items={item}

                                /> */}
                                <ul className='nav-top'>
                                    {
                                        second_level_router.map((item, index) => {
                                            return <li key={index}><NavLink to={item.path}>{item.title}</NavLink></li>
                                        })
                                    }
                                </ul>
                                <div className='right_Drawer'>
                                    <Space direction="vertical">
                                        <Space wrap>
                                            <Dropdown overlay={menu} placement="bottomLeft">
                                                <Button >
                                                    {/* @ts-ignore  */}
                                                    <svg t="1663243557376" className="icon" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="2552" width="24" height="24"><path d="M651.3 390.041h325.224a47.76 47.76 0 0 1 46.623 48.897v536.165A47.76 47.76 0 0 1 976.524 1024H419.322a47.76 47.76 0 0 1-46.623-48.897V633.959H46.907A47.76 47.76 0 0 1 0.284 585.062V48.897A47.76 47.76 0 0 1 46.907 0h557.77A47.76 47.76 0 0 1 651.3 48.897zM46.907 585.061h557.77V48.898H46.907v536.165z m635.665-16.488l-102.343 278.6h45.485l25.018-71.64h113.714l25.018 71.64h43.78l-102.912-278.6z m-22.175 170.572l44.35-126.223 47.19 126.223z m-354.22-625.43h35.251v71.071h135.32v188.198h-35.251V350.81H341.428V481.58h-33.546V350.81h-97.226v22.174h-35.251V184.786h132.477z m-95.52 200.706h96.657v-93.246h-96.658z m130.771 0h100.069v-93.246H341.428z" p-id="2553"></path></svg>

                                                </Button>
                                            </Dropdown>

                                        </Space>
                                    </Space>
                                    {/* @ts-ignore  */}
                                    {window.localStorage.getItem('theme') === 'false' ? <svg style={{ cursor: 'pointer', margin: '10px' }} t="1663659043855" onClick={() => { dians(); console.log(theme) }} className="top_icon" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="9136" width="25" height="25"><path d="M501.48 493.55m-233.03 0a233.03 233.03 0 1 0 466.06 0 233.03 233.03 0 1 0-466.06 0Z" fill="#F9C626" p-id="9137"></path><path d="M501.52 185.35H478.9c-8.28 0-15-6.72-15-15V87.59c0-8.28 6.72-15 15-15h22.62c8.28 0 15 6.72 15 15v82.76c0 8.28-6.72 15-15 15zM281.37 262.76l-16 16c-5.86 5.86-15.36 5.86-21.21 0l-58.52-58.52c-5.86-5.86-5.86-15.36 0-21.21l16-16c5.86-5.86 15.36-5.86 21.21 0l58.52 58.52c5.86 5.86 5.86 15.35 0 21.21zM185.76 478.48v22.62c0 8.28-6.72 15-15 15H88c-8.28 0-15-6.72-15-15v-22.62c0-8.28 6.72-15 15-15h82.76c8.28 0 15 6.72 15 15zM270.69 698.63l16 16c5.86 5.86 5.86 15.36 0 21.21l-58.52 58.52c-5.86 5.86-15.36 5.86-21.21 0l-16-16c-5.86-5.86-5.86-15.36 0-21.21l58.52-58.52c5.85-5.86 15.35-5.86 21.21 0zM486.41 794.24h22.62c8.28 0 15 6.72 15 15V892c0 8.28-6.72 15-15 15h-22.62c-8.28 0-15-6.72-15-15v-82.76c0-8.28 6.72-15 15-15zM706.56 709.31l16-16c5.86-5.86 15.36-5.86 21.21 0l58.52 58.52c5.86 5.86 5.86 15.36 0 21.21l-16 16c-5.86 5.86-15.36 5.86-21.21 0l-58.52-58.52c-5.86-5.85-5.86-15.35 0-21.21zM802.17 493.59v-22.62c0-8.28 6.72-15 15-15h82.76c8.28 0 15 6.72 15 15v22.62c0 8.28-6.72 15-15 15h-82.76c-8.28 0-15-6.72-15-15zM717.24 273.44l-16-16c-5.86-5.86-5.86-15.36 0-21.21l58.52-58.52c5.86-5.86 15.36-5.86 21.21 0l16 16c5.86 5.86 5.86 15.36 0 21.21l-58.52 58.52c-5.86 5.86-15.35 5.86-21.21 0z" fill="#F9C626" p-id="9138"></path></svg> : <svg style={{ cursor: 'pointer' }} t="1663659688874" onClick={() => { dians(); console.log(theme) }} className="top_icon" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="10313" width="25" height="25"><path d="M529.611373 1023.38565c-146.112965 0-270.826063-51.707812-374.344078-155.225827C51.74928 764.641808 0.041469 639.826318 0.041469 493.815745c0-105.053891 29.693595-202.326012 88.978393-292.22593 59.38719-89.797526 137.000103-155.942569 232.83874-198.63991 6.041111-4.607627 12.184613-3.788493 18.225724 2.252618 7.576986 4.607627 9.931996 11.365479 6.860244 20.580733C322.677735 83.736961 310.493122 142.202626 310.493122 201.589815c0 135.464227 48.328885 251.474031 144.986656 348.131801 96.657771 96.657771 212.667574 144.986656 348.131801 144.986656 74.541162 0 139.252721-11.365479 194.032283-34.19883C1003.684974 655.799424 1009.726084 656.618558 1015.767195 662.659669c7.576986 4.607627 9.931996 11.365479 6.860244 20.580733C983.104241 786.758417 918.802249 869.286132 829.721465 930.925939 740.743072 992.565746 640.706375 1023.38565 529.611373 1023.38565z" p-id="10314" fill="#d4237a"></path></svg>
                                    }   <SearchOutlined onClick={() => {
                                        setFlag(true)
                                    }} className='icon' style={{ width: "20px", height: "20px" }} />
                                </div>
                            </Drawer>
                        </>
                        {/* 切换主题 */}

                    </div>
                </Header>
            </Affix>
            {/* 搜索遮罩层 */}
            {
                flag ? <div className='archives_mask' >
                    <div className='archives_mask_div'>
                        <span className='archives_searchArticle'>searchArticle </span>

                        <div onClick={() => {
                            setFlag(false)
                        }} >
                            <p className='archives_x' >x</p>
                            <p>esx</p></div>
                    </div>
                    <Space direction="vertical">
                        <Search placeholder="searchArticlePlaceholder" onSearch={onSearch} />
                    </Space>
                    <div className='archives_search'>

                        {
                            search_data ? <ul>
                                {
                                    search_data.map((item: DATA, index: number) => {
                                        return <li onClick={() => { navigate('/detail/' + item.id, { state: item }) }} key={index}>
                                            {item.title}

                                        </li>
                                    })
                                }
                            </ul> : 'null'
                        }
                    </div>
                </div> : null
            }
        </div>
    )
}



export default SmallHeaders