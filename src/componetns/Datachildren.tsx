import React, { useState, useCallback, useEffect } from 'react'
import { Button, Col, Row } from 'antd';
import { AudioOutlined, WifiOutlined, GithubOutlined } from '@ant-design/icons';
import { second_level_router } from "../router/Routerconfig";
import { Outlet, useNavigate, useLocation } from 'react-router-dom';
import { NavLink } from 'react-router-dom';
import RecommendToReading from './RecommendToReading';
import './components/Appbox.scss'
import Headers from "./header/Headers";
import style from "../styles/style.module.css";
import { BackTop } from 'antd';
import './components/Datachildren.css'
import { Tab_tepl, Datachildren_setistitle, Datachildren_setdatas, Datachildren_setdatase } from '../page/category/aps'
// import '../page/home/index/article/component/Article.css'
import ArticleChile from '../page/home/index/article/component/ArticleChile';
//判断当前时间
const item = second_level_router.map((item, index) => {
  const key = index + 1;
  const nav = <NavLink to={item.path}>{item.title}</NavLink>
  return {
    key,
    label: nav,
  };
})
function Datachildren() {
  const navigate = useNavigate()
  const location = useLocation()
  const [istitle, setistitle] = useState([])
  const [datas, setdatas] = useState([])
  const [datase, setdatase] = useState([])
  const IStitle = async () => {
    let das = await Datachildren_setistitle()
    setistitle(das.data.data)
  }
  const DAtas = async () => {
    let das = await Datachildren_setdatas()
    setdatas(das.data.data)
  }
  const DAtase = async () => {
    const das = await Datachildren_setdatase()
    setdatase(das.data.data)
  }
  const assd = async (id: any) => {
    const res2 = await Tab_tepl(id.value)
    setdatatitle(res2.data.data[0])
    console.log(res2);
  }

  useEffect(() => {
    assd(location.state as any);
    IStitle();
    DAtas();
    DAtase()
  }, [])
  const [datatitle, setdatatitle] = useState([])
  let inte = async (item: any) => {
    const add = await Tab_tepl(item.value)
    setdatatitle(add.data.data[0])
    navigate(`/home/article/DataChildren/${item.label}`, {
      state: item
    })
    console.log(item, 100);

    console.log(location.state, 100);

  }
  var moment = require('moment');
  moment.defineLocale('zh-cn', {
    relativeTime: {
      future: "%s内",
      past: "%s前",
      s: '几秒',
      m: "一分钟",
      mm: "%d分钟",
      h: "1小时",
      hh: "%d小时",
      d: "1天",
      dd: "%d天",
      M: "1月",
      MM: "%d个月",
      y: "1年",
      yy: "%d年"
    }
  })
  //判断当前时间


  const [fage, setfage] = useState(false)
  const [archives_chil, setarchives_chil] = useState()
  const showModal = (item: any) => {
    setfage(true)
    setarchives_chil(item)
  }
  const show = (show: any) => {
    setfage(show)
  }
  return (
    <div className={style.note}>
      <div className='apbox'>
        <BackTop />
        <Headers />
        <main>
          <Row>
            <Col span={3}></Col>
            <Col span={13}>
              <div className='main'>
                {/* 占位盒子 */}
                <div children className='sup'></div>
                <div className='op'>
                  <div >
                    {
                      fage ? <ArticleChile archives_chil={archives_chil} show={show}></ArticleChile> : null
                    }
                    {
                      datase.length ? datase.map((item: any, index: number) => {
                        if (item.label == (location.state as any).label) {
                          return (
                            <span key={index}
                            >
                              <span style={{ color: "red" }}>{item.label}</span>
                              <span>tag related articles</span>
                              <br />
                              <span>Totally searched </span>
                              <span style={{ color: "red" }}>{item.articleCount}</span>
                              <span>Count</span>
                            </span>
                          )
                        }
                      }) : ""
                    }
                  </div>
                  <div className='main_emsp'>
                    &emsp;
                  </div>
                  <div className='Datachildren_DIV'>
                    {
                      <RecommendToReading str={"Tag Title"}>
                        {
                          datase.length ? datase.map((item: any, index: number) => {
                            <br />
                            return (
                              <span key={index}
                                onClick={() => inte(item)}
                              >
                                <Button style={{
                                  color: item.label == (location.state as any).label ? "#fff" : '',
                                  background: item.label == (location.state as any).label ? "#f40a12" : "", display: 'inline-block'
                                }}>
                                  {item.label}
                                  [{item.articleCount}]
                                </Button>
                                &emsp;
                              </span>
                            )
                          }) : ""
                        }
                      </RecommendToReading>
                    }
                  </div>
                  <div className='main_emsp'>
                    &emsp;
                  </div>
                  <div>
                    {
                      datatitle.length ? datatitle.map((item: any, index: number) => {
                        return (
                          <div key={index} className="liaos" style={{ flexWrap: 'wrap', borderBottom: "1px solid #ccc" }}>
                            <p style={{ width: "100%" }}><b onClick={() => {
                              navigate(`/detail/${item.id}`, {
                                state: item
                              })
                            }}>{item.title}</b> &ensp;<span>|&ensp;{moment(item.createAt).fromNow()}</span> &emsp;<span>{item.tags.length ? item.tags[0].label : null}</span></p>
                            <dl>
                              <dt>
                                {
                                  item.cover ? <img src={item.cover} alt="" /> : <img src='https://bwcreation.oss-cn-beijing.aliyuncs.com/2022-09-06/%E5%B0%8F%E7%86%8A.jpg' alt="" />
                                }
                              </dt>
                              {/* {
                                item.summary ? <dd className='liao_dd'>{item.summary}</dd> : <span style={{ margin: "0 0 80px 0", width: "50%", display: "block" }}></span>
                              } */}

                              {/* <br /> */}
                              <dd>
                                <p>
                                  <p>{item.summary}</p>
                                  <br />
                                  <br />
                                  <span>
                                    <span>
                                      {/* @ts-ignore */}
                                      <svg t="1663587350929" className="icon" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="2606" width="14" height="14"><path d="M859.733333 253.866667c-44.8-44.8-102.4-70.4-166.4-70.4-61.866667 0-121.6 25.6-166.4 70.4l-14.933333 17.066666-17.066667-17.066666c-44.8-44.8-102.4-70.4-166.4-70.4-61.866667 0-121.6 25.6-166.4 70.4-91.733333 91.733333-91.733333 243.2 0 337.066666l324.266667 330.666667c6.4 6.4 14.933333 8.533333 23.466667 8.533333s17.066667-4.266667 23.466666-8.533333l324.266667-330.666667c44.8-44.8 68.266667-104.533333 68.266667-168.533333s-21.333333-123.733333-66.133334-168.533333z m-44.8 290.133333L512 853.333333 209.066667 544c-66.133333-68.266667-66.133333-179.2 0-247.466667 32-32 74.666667-51.2 119.466666-51.2 44.8 0 87.466667 17.066667 119.466667 51.2l38.4 40.533334c12.8 12.8 34.133333 12.8 44.8 0l38.4-40.533334c32-32 74.666667-51.2 119.466667-51.2 44.8 0 87.466667 17.066667 119.466666 51.2 32 32 49.066667 76.8 49.066667 123.733334s-12.8 91.733333-42.666667 123.733333z" p-id="2607"></path></svg>

                                    </span>
                                    <span className="know_span_svg">
                                      {item.likes}
                                    </span>
                                  </span>
                                  <span style={{ color: '#8590a6' }}>
                                    &ensp;·&ensp;
                                  </span>
                                  <span>
                                    <span>
                                      {/* @ts-ignore */}
                                      <svg t="1663299295299" className="icon" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="2416" width="14" height="14"><path d="M515.2 224c-307.2 0-492.8 313.6-492.8 313.6s214.4 304 492.8 304 492.8-304 492.8-304S822.4 224 515.2 224zM832 652.8c-102.4 86.4-211.2 140.8-320 140.8s-217.6-51.2-320-140.8c-35.2-32-70.4-64-99.2-99.2-6.4-6.4-9.6-12.8-16-19.2 3.2-6.4 9.6-12.8 12.8-19.2 25.6-35.2 57.6-70.4 92.8-102.4 99.2-89.6 208-144 329.6-144s230.4 54.4 329.6 144c35.2 32 64 67.2 92.8 102.4 3.2 6.4 9.6 12.8 12.8 19.2-3.2 6.4-9.6 12.8-16 19.2C902.4 585.6 870.4 620.8 832 652.8z" p-id="2417" fill="#bfbfbf"></path><path d="M512 345.6c-96 0-169.6 76.8-169.6 169.6 0 96 76.8 169.6 169.6 169.6 96 0 169.6-76.8 169.6-169.6C681.6 422.4 604.8 345.6 512 345.6zM512 640c-67.2 0-121.6-54.4-121.6-121.6 0-67.2 54.4-121.6 121.6-121.6 67.2 0 121.6 54.4 121.6 121.6C633.6 582.4 579.2 640 512 640z" p-id="2418" fill="#bfbfbf"></path></svg>
                                    </span>
                                    <span className="know_span_svg">
                                      {item.views}
                                    </span>
                                  </span>
                                  <span style={{ color: '#8590a6' }}>
                                    &ensp;·&ensp;
                                  </span>
                                  <span
                                    onClick={() => showModal(item)}
                                  >
                                    <span>
                                      {/* @ts-ignore */}
                                      <svg t="1663246548343" className="icon" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="4150" width="14" height="14"><path d="M763.84 896c-47.128 0-85.333-38.205-85.333-85.333s38.205-85.333 85.333-85.333c47.128 0 85.333 38.205 85.333 85.333 0 47.128-38.205 85.333-85.333 85.333M329.92 558.848c-14.895 26.231-42.641 43.638-74.453 43.638-47.128 0-85.333-38.205-85.333-85.333 0-16.097 4.457-31.152 12.204-44 14.935-24.769 42.098-41.333 73.13-41.333 47.128 0 85.333 38.205 85.333 85.333 0 15.317-4.035 29.691-11.101 42.117M763.84 128c47.128 0 85.333 38.205 85.333 85.333s-38.205 85.333-85.333 85.333c-47.128 0-85.333-38.205-85.333-85.333 0-47.128 38.205-85.333 85.333-85.333M763.84 682.667c-0.021 0-0.047 0-0.072 0-39.16 0-74.203 17.626-97.628 45.378l-289.885-167.063c4.932-13.101 7.787-28.245 7.787-44.055 0-0.105 0-0.209 0-0.314 0-0.072 0-0.177 0-0.281 0-15.81-2.855-30.953-8.077-44.942l295.544-169.566c23.265 24.363 56.001 39.509 92.275 39.509 0.020 0 0.039 0 0.059 0 70.689 0 127.997-57.308 127.997-128 0-70.692-57.308-128-128-128-70.692 0-128 57.308-128 128 0 18.965 4.224 36.907 11.627 53.099l-292.288 168.747c-23.653-28.833-59.285-47.084-99.18-47.084-70.692 0-128 57.308-128 128 0 0.188 0 0.376 0.001 0.564-0.001 0.123-0.001 0.304-0.001 0.484 0 70.692 57.308 128 128 128 39.895 0 75.526-18.251 99.001-46.86l289.373 166.752c-5.397 13.568-8.529 29.29-8.533 45.743 0 70.582 57.308 127.889 128 127.889 70.692 0 128-57.308 128-128 0-70.692-57.308-128-128-128z" fill="#8a8a8a" p-id="4151"></path></svg>
                                    </span>
                                    <span className="know_span_svg">
                                      share
                                    </span>

                                  </span>
                                </p>
                              </dd>
                            </dl>
                          </div>
                        )
                      }) : <div style={{ width: '100%', height: '100px', color: "#ccc", lineHeight: "100px" }}>暂无数据</div>
                    }
                  </div>
                </div>
              </div>
            </Col>
            <Col span={5}>
              <div className='vice'>
                {/* 中间右上组件 */}
                <RecommendToReading str={"recommend ToReading"}>
                  {
                    datas.length ? datas.map((item: any, index: number) => {
                      return (
                        <li key={index}
                          onClick={() => {
                            navigate(`/detail/${item.id}`, {
                              state: item
                            })
                          }}
                        >
                          <span className='datas_span'>{item.title}</span>
                          <span>&ensp;</span>
                          <span style={{ color: "#ccc" }}>{moment(item.createAt).fromNow()}</span>
                        </li>
                      )
                    }) : ""
                  }
                </RecommendToReading>
                {/* 中间右下组件 */}
                {
                  <RecommendToReading str={"category Title"}>
                    {
                      istitle.length ? istitle.map((item: any, index: number) => {
                        return (
                          <p key={index}
                            onClick={() => {
                              navigate(`/home/article/category/${item.label}`, {
                                state: item
                              })
                            }}
                            className='Appbox_p'

                          >
                            <span>{item.label}</span>
                            <span style={{ float: 'right', marginRight: '10px' }}>共计{item.articleCount}篇文章</span>

                          </p>
                        )
                      }) : ""
                    }
                  </RecommendToReading>
                }
              </div>
            </Col>
            <Col span={3}></Col>
          </Row>
        </main>
        <>
          <BackTop />
        </>
        <footer>
          <div className='footerdiv'>
            <WifiOutlined style={{ color: '#fff', fontSize: '30px' }} />
            <GithubOutlined style={{ color: '#fff', fontSize: '30px' }} />
          </div>
        </footer>
      </div>
    </div>
  )
}
export default Datachildren
