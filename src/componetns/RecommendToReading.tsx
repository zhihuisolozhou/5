import React, { useState, useEffect } from "react";
import { Empty, Spin } from "antd";
import "./components/Appbox.css";
function RecommendToReading(props: any) {
  let { str } = props;
  return (
    <div className="slottal_head_div">
      <h5 style={{ fontSize: "14px" }} className="slottal_head_h4">
        {str}
      </h5>
      <div>
        {props.children.length ? (
          props.children.map((item: any, index: number) => {
            if (item.type) {
              return (
                <item.type
                  key={index}
                  onClick={() => {
                    item.props.onClick();
                  }}
                  style={{
                    animation: `example1 ${(index + 1) * 0.1}s ease-out ${
                      (index + 2) * 0.1
                    }s backwards`,
                    height: "30px",
                    LineHeight: "30px",
                  }}
                  className="Appbox_p"
                >
                  {item.props.children}
                </item.type>
              );
            }
            return <li key={index}>{item}</li>;
          })
        ) : (
          <div className="example" style={{ background: "#fff" }}>
            <Spin />
          </div>
        )}
      </div>
    </div>
  );
}

export default RecommendToReading;
