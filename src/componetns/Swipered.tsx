import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { ALLSTATE } from "../types/store.d";
import moment from "moment";
import { useNavigate } from "react-router-dom";
import { Navigation, Pagination, A11y, Autoplay } from "swiper";
import { Swiper, SwiperSlide } from "swiper/react";
import { Empty } from "antd";
import { Image } from "react-vant";

import "swiper/css";
import "swiper/css/navigation";
import "swiper/css/pagination";
import "swiper/css/scrollbar";

function Swipered() {
  const dispatch = useDispatch();
  const navigate = useNavigate();

  //时间格式配置  几小时前
  moment.defineLocale("zh-cn", {
    relativeTime: {
      future: "%s内",
      past: "%s前",
      s: "几秒",
      m: "一分钟",
      mm: "%d分钟",
      h: "1小时",
      hh: "%d小时",
      d: "1天",
      dd: "%d天",
      M: "1月",
      MM: "%d个月",
      y: "1年",
      yy: "%d年",
    },
  });
  {
    /* @ts-ignore  */
  }
  const swiperlist = useSelector((state: ALLSTATE) => state.redux.swiperlist);
  useEffect(() => {
    fetch("https://creationapi.shbwyz.com/api/article?page=1&pageSize=12")
      .then(res => res.json())
      .then(res => {
        dispatch({
          type: "SWIPEREDLIST",
          payload: res.data[0],
        });
        // console.log(res.data,'所有文章');
      });
  }, []);

  console.log(swiperlist);

  //点击轮播图跳转详情
  const godetail = (it: any) => {
    dispatch({
      type: "SETVIEWS",
      payload: it,
    });
    navigate("/detail/" + it.id, { state: it });
  };

  return (
    <>
      <Swiper
        // install Swiper modules
        modules={[Navigation, Pagination, A11y, Autoplay]}
        spaceBetween={50}
        slidesPerView={1}
        navigation
        autoplay={
          swiperlist.length == 1
            ? false
            : {
              delay: 3000,
              disableOnInteraction: false,
            }
        }
        loop={true}
        // allowSlideNext={false}
        // allowSlidePrev={false}
        pagination={{ clickable: true }}
        scrollbar={{ draggable: true }}
        onSwiper={swiper => console.log(swiper)}
        onSlideChange={() => console.log("slide change")}
      >
        {swiperlist && swiperlist.length > 0 ? (
          swiperlist.map((item: any, index: number) => {
            return (
              <SwiperSlide key={index}>
                <Image
                  width="100%"
                  height="100%"
                  fit="cover"
                  src={item.cover}
                />
                {/* <img id='imgswiper' className='swiperimages' src={item.cover} alt="" /> */}
                <a className="Abox" onClick={() => godetail(item)}>
                  <div className="midbox">
                    <p></p>
                    <p></p>
                    <p></p>
                    <p></p>
                    <h2 className="swiperP1">{item.title}</h2>
                    <p className="swiperP2">
                      {/* <time dateTime={item.updateAt}></time> */}
                      <span>大约 {moment(item.createAt).fromNow()}</span> ·{" "}
                      <span>{item.views} readingCountTemplate</span>
                    </p>
                    <p></p>
                    <p></p>
                    <p></p>
                    <p></p>
                  </div>
                </a>
              </SwiperSlide>
            );
          })
        ) : (
          <Empty />
        )}
      </Swiper>
    </>
  );
}

export default Swipered;
