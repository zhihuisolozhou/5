import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { Avatar } from "antd";
import moment from "moment";
import { Input, Button, message, Col, Row } from "antd";
import { Modal, Form } from "antd";
import Textvalue from "./Textvalue";
moment.defineLocale("zh-cn", {
    relativeTime: {
        future: "%s内",
        past: "%s前",
        s: "几秒",
        m: "一分钟",
        mm: "%d分钟",
        h: "1小时",
        hh: "%d小时",
        d: "1天",
        dd: "%d天",
        M: "1月",
        MM: "%d个月",
        y: "1年",
        yy: "%d年",
    },
});

function Comment({ id, colorac, list, marleft, names }: any) {
    const dispatch = useDispatch();
    const [textValue, settextValue] = useState<string>("");
    const [isInputOpen, setisInputOpen] = useState<boolean>(false);
    const [isEmojiOpen, setisEmojiOpen] = useState<boolean>(false);
    const emojilist = [
        { id: 1, emoji: "😀" },
        { id: 2, emoji: "😂" },
        { id: 3, emoji: "🙂" },
        { id: 4, emoji: "😁" },
        { id: 5, emoji: "😊" },
        { id: 6, emoji: "👩‍🦲" },
        { id: 7, emoji: "👩" },
        { id: 8, emoji: "👨" },
        { id: 9, emoji: "👵" },
        { id: 10, emoji: "👴" },
        { id: 11, emoji: "🐖" },
        { id: 12, emoji: "🐑" },
        { id: 13, emoji: "🐭" },
        { id: 14, emoji: "🐷" },
        { id: 15, emoji: "🐓" },
        { id: 16, emoji: "🍌" },
        { id: 17, emoji: "🍉" },
        { id: 18, emoji: "🍒" },
        { id: 19, emoji: "🥕" },
        { id: 20, emoji: "🥒" },
        // { id: 21, emoji: '👊👴👊' },
        //   .👊👴👊
        //     🎽
        //     👖
        // 👞  👞
    ];
    const inputshowModal = () => {
        setisInputOpen(true);
    };

    const inputhandleOk = () => {
        setisInputOpen(false);
    };

    const inputhandleCancel = () => {
        setisInputOpen(false);
    };

    const inputsharetrue = () => {
        setisInputOpen(true);
    };

    const inputfocus = () => {
        if (!window.localStorage.getItem("emailtoken")) {
            inputshowModal();
        }
    };

    // 表情
    const emojishowModal = () => {
        // 判断是否邮箱登录
        if (!window.localStorage.getItem("emailtoken")) {
            inputshowModal();
        } else {
            setisEmojiOpen(true);
        }
    };
    // 点击emoji
    const emojihandleOk = (item: any) => {
        setisEmojiOpen(false);
        settextValue(textValue + item);
    };

    const emojihandleCancel = () => {
        setisEmojiOpen(false);
    };

    const onChange = (e: any) => {
        settextValue(e.target.value);
    };

    const sendmessage = () => {
        dispatch({
            type: "ADDTALKS",
            payload: { textValue, id },
        });
        settextValue("");
        message.success("评论成功！");
    };

    const onFinish = (values: any) => {
        console.log("Success:", values);
        window.localStorage.setItem("emailtoken", JSON.stringify(values));
        setisInputOpen(false);
        message.success("邮箱登录成功");
    };

    const onFinishFailed = (errorInfo: any) => {
        console.log("Failed:", errorInfo);
    };
    return (
        <>
            {list &&
                list.map((item: any, index: number) => {
                    return (
                        <div
                            key={index}
                            style={{ width: "100%", height: "auto", textAlign: "left" }}
                        >
                            {/* <div className='budthree' style={{ width: '100%', height: '15px', background: '#e7eaee' }}></div> */}
                            <div
                                className="headers"
                                style={{
                                    marginLeft: marleft,
                                    width: "100%",
                                    height: "50px",
                                    display: "flex",
                                    alignItems: "center",
                                }}
                            >
                                <Avatar
                                    style={{ marginLeft: "20px", background: `${colorac}` }}
                                >
                                    {item.name.slice(0, 1)}
                                </Avatar>
                                <span
                                    style={{
                                        marginLeft: "15px",
                                        fontWeight: "bold",
                                        fontSize: "14px",
                                    }}
                                >
                                    {names ? item.name + "  回复  " + names : item.name}
                                </span>
                            </div>
                            <div
                                className="mains"
                                style={{
                                    marginLeft: marleft,
                                    width: "100%",
                                    height: "50px",
                                    display: "flex",
                                    alignItems: "center",
                                }}
                            >
                                <p style={{ marginLeft: "20px", fontSize: "16px" }}>
                                    {item.content}
                                </p>
                            </div>
                            <div
                                className="footers"
                                style={{
                                    marginLeft: marleft,
                                    color: "#666",
                                    width: "100%",
                                    height: "50px",
                                    display: "flex",
                                    alignItems: "center",
                                }}
                            >
                                <span style={{ marginLeft: "20px" }}>{item.userAgent} </span>
                                <span style={{ marginLeft: "8px" }}>
                                    {moment(item.createAt).fromNow()}{" "}
                                </span>
                                <Textvalue user={item.name} id={item.id}></Textvalue>
                            </div>
                            {item.children && (
                                <Comment
                                    names={item.name}
                                    marleft={"30px"}
                                    list={item.children}
                                ></Comment>
                            )}
                            {!names ? (
                                <div
                                    className="budtwo"
                                    style={{
                                        width: "100%",
                                        height: "16px",
                                        background: "#e7eaee",
                                    }}
                                ></div>
                            ) : (
                                ""
                            )}
                        </div>
                    );
                })}

            {/* 评论登录 */}
            <div>
                <Modal
                    className="talklogin"
                    cancelText={"-"}
                    okText={"-"}
                    okType={"text"}
                    title="Basic Modal"
                    open={isInputOpen}
                    onOk={inputhandleOk}
                    onCancel={inputhandleCancel}
                >
                    <Form
                        name="basic"
                        labelCol={{ span: 8 }}
                        wrapperCol={{ span: 16 }}
                        initialValues={{ remember: true }}
                        onFinish={onFinish}
                        onFinishFailed={onFinishFailed}
                        autoComplete="off"
                    >
                        <Form.Item
                            label="Name"
                            name="name"
                            rules={[{ required: true, message: "Please input your Name!" }]}
                        >
                            <Input />
                        </Form.Item>

                        <Form.Item
                            label="Email"
                            name="email"
                            rules={[
                                { type: "email", message: "The input is not valid E-mail!" },
                                { required: true, message: "Please input your Email!" },
                            ]}
                        >
                            <Input />
                        </Form.Item>

                        <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
                            <Button type="primary" htmlType="submit">
                                Submit
                            </Button>
                        </Form.Item>
                    </Form>
                </Modal>
            </div>

            {/* 表情 */}
            <>
                <Modal
                    style={{ top: "400px", left: "100px" }}
                    title="emojiicon"
                    open={isEmojiOpen}
                    onOk={() => emojihandleOk("")}
                    onCancel={emojihandleCancel}
                >
                    <Row gutter={0}>
                        {emojilist &&
                            emojilist.map((item, index) => {
                                return (
                                    <Col
                                        style={{ margin: "6px", background: "none" }}
                                        key={index}
                                        span={2}
                                        onClick={() => emojihandleOk(item.emoji)}
                                    >
                                        <div style={{ cursor: "pointer" }}>{item.emoji}</div>
                                    </Col>
                                );
                            })}
                    </Row>
                </Modal>
            </>
        </>
    );
}

export default Comment;
