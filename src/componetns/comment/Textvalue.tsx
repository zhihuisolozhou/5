import React, { useEffect, useState } from "react";
import { Input, Button, message, Col, Row } from "antd";
import { SmileFilled } from "@ant-design/icons";
import { Modal, Form } from "antd";
import { Dropdown } from "antd";
import { MessageOutlined } from "@ant-design/icons";
import { useDispatch } from "react-redux";

const { TextArea } = Input;

function Textvalue({ id, user }: any) {
  const dispatch = useDispatch();
  const [textValue, settextValue] = useState<string>("");
  const [isInputOpen, setisInputOpen] = useState<boolean>(false);
  const [isEmojiOpen, setisEmojiOpen] = useState<boolean>(false);
  const [open, setOpen] = useState(false);
  const emojilist = [
    { id: 1, emoji: "😀" },
    { id: 2, emoji: "😂" },
    { id: 3, emoji: "🙂" },
    { id: 4, emoji: "😁" },
    { id: 5, emoji: "😊" },
    { id: 6, emoji: "👩‍🦲" },
    { id: 7, emoji: "👩" },
    { id: 8, emoji: "👨" },
    { id: 9, emoji: "👵" },
    { id: 10, emoji: "👴" },
    { id: 11, emoji: "🐖" },
    { id: 12, emoji: "🐑" },
    { id: 13, emoji: "🐭" },
    { id: 14, emoji: "🐷" },
    { id: 15, emoji: "🐓" },
    { id: 16, emoji: "🍌" },
    { id: 17, emoji: "🍉" },
    { id: 18, emoji: "🍒" },
    { id: 19, emoji: "🥕" },
    { id: 20, emoji: "🥒" },
    // { id: 21, emoji: '👊👴👊' },
    //   .👊👴👊
    //     🎽
    //     👖
    // 👞  👞
  ];
  const inputshowModal = () => {
    setisInputOpen(true);
  };

  const inputhandleOk = () => {
    setisInputOpen(false);
  };

  const inputhandleCancel = () => {
    setisInputOpen(false);
  };

  const inputsharetrue = () => {
    setisInputOpen(true);
  };

  const inputfocus = () => {
    if (!window.localStorage.getItem("emailtoken")) {
      inputshowModal();
    }
  };

  // 表情
  const emojishowModal = () => {
    // 判断是否邮箱登录
    if (!window.localStorage.getItem("emailtoken")) {
      inputshowModal();
    } else {
      setisEmojiOpen(true);
    }
  };
  // 点击emoji
  const emojihandleOk = (item: any) => {
    setOpen(true);
    setisEmojiOpen(false);
    settextValue(textValue + item);
  };

  const emojihandleCancel = () => {
    setisEmojiOpen(false);
  };

  const onChange = (e: any) => {
    settextValue(e.target.value);
  };

  const sendmessage = () => {
    dispatch({
      type: "ADDCOMMENT",
      payload: { textValue, id },
    });
    settextValue("");
    message.success("评论成功！");
    setOpen(false);
  };

  const onFinish = (values: any) => {
    console.log("Success:", values);
    window.localStorage.setItem("emailtoken", JSON.stringify(values));
    setisInputOpen(false);
    message.success("邮箱登录成功");
  };

  const onFinishFailed = (errorInfo: any) => {
    console.log("Failed:", errorInfo);
  };

  const handleOpenChange = (flag: boolean) => {
    setOpen(!open);
  };

  return (
    <div>
      <Dropdown
        onOpenChange={handleOpenChange}
        open={open}
        overlay={
          <div style={{ background: "#fff", borderRadius: "10px" }} id="talk">
            {/* text框 */}
            <TextArea
              style={{ width: "100%", borderRadius: "10px" }}
              onClick={() => inputfocus()}
              className="textarea"
              value={textValue}
              onChange={onChange}
              rows={4}
              placeholder={"回复 " + user}
              maxLength={100}
            />
            <div className="publishbox" style={{ marginTop: "12px" }}>
              {/* 表情 */}
              <div className="smilehover" onClick={emojishowModal}>
                <SmileFilled />
                <span>emoji</span>
              </div>
              {/* 输入框内容为空的话不能提交 */}
              {textValue.length > 0 ? (
                <Button
                  onClick={() => sendmessage()}
                  style={{ marginLeft: "120px" }}
                  type="primary"
                  className="button"
                  size="middle"
                >
                  commentNamespace.publish
                </Button>
              ) : (
                <Button
                  disabled
                  onClick={() => sendmessage()}
                  style={{ marginLeft: "120px" }}
                  type="primary"
                  className="button"
                  size="middle"
                >
                  commentNamespace.publish
                </Button>
              )}
            </div>
          </div>
        }
        trigger={["click"]}
      >
        <span
          onClick={e => e.preventDefault()}
          className="spanpl"
          style={{ marginLeft: "8px" }}
        >
          <MessageOutlined /> commentNamespace.reply
        </span>
      </Dropdown>

      {/* 评论登录 */}
      <div>
        <Modal
          className="talklogin"
          cancelText={"-"}
          okText={"-"}
          okType={"text"}
          title="Basic Modal"
          open={isInputOpen}
          onOk={inputhandleOk}
          onCancel={inputhandleCancel}
        >
          <Form
            name="basic"
            labelCol={{ span: 8 }}
            wrapperCol={{ span: 16 }}
            initialValues={{ remember: true }}
            onFinish={onFinish}
            onFinishFailed={onFinishFailed}
            autoComplete="off"
          >
            <Form.Item
              label="Name"
              name="name"
              rules={[{ required: true, message: "Please input your Name!" }]}
            >
              <Input />
            </Form.Item>

            <Form.Item
              label="Email"
              name="email"
              rules={[
                { type: "email", message: "The input is not valid E-mail!" },
                { required: true, message: "Please input your Email!" },
              ]}
            >
              <Input />
            </Form.Item>

            <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
              <Button type="primary" htmlType="submit">
                Submit
              </Button>
            </Form.Item>
          </Form>
        </Modal>
      </div>

      {/* 表情 */}
      <>
        <Modal
          style={{ top: "400px", left: "100px" }}
          title="emojiicon"
          open={isEmojiOpen}
          onOk={() => emojihandleOk("")}
          onCancel={emojihandleCancel}
        >
          <Row gutter={0}>
            {emojilist &&
              emojilist.map((item, index) => {
                return (
                  <Col
                    style={{ margin: "6px", background: "none" }}
                    key={index}
                    span={2}
                    onClick={() => emojihandleOk(item.emoji)}
                  >
                    <div style={{ cursor: "pointer" }}>{item.emoji}</div>
                  </Col>
                );
              })}
          </Row>
        </Modal>
      </>
    </div>
  );
}

export default Textvalue;
