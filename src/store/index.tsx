import { legacy_createStore, applyMiddleware, combineReducers } from "redux";
import logger from "redux-logger";
import thunk from "redux-thunk";
import redux from "./user/redux";
import knowledge from "./knowledge/redux";
const ALL_reducer = combineReducers({
  redux,
  knowledge,
});
export default legacy_createStore(ALL_reducer, applyMiddleware(logger, thunk));
