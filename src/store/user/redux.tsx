import { DATA } from "../../page/home/index/archives/type/stype.d";

const initialState = {
  get_data: [],
  publish_data: [],
  search_data: [],
  talkslist:
    JSON.parse(window.localStorage.getItem("talkslist") as string) || [],
  // talkslist: [],
  swiperlist:
    JSON.parse(window.localStorage.getItem("swiperlist") as string) || [],
  commentlist:
    JSON.parse(window.localStorage.getItem("commentlist") as string) || [],
  copycommentlist:
    JSON.parse(window.localStorage.getItem("commentlist") as string) || [],
};

export default (state: any = initialState, { type, payload }: any) => {
  const newState = JSON.parse(JSON.stringify(state));

  const setlocalStorage = (name: string, data: any) => {
    window.localStorage.setItem(name, JSON.stringify(data));
  };

  switch (type) {
    case "SWIPEREDLIST":
      let newswiper = [...state.swiperlist];
      payload.forEach((item: any) => {
        const flag = newState.swiperlist.some((it: any) => it.id === item.id);
        // 不为null 状态为发布 推荐为true 就push
        if (
          !flag &&
          item.cover !== null &&
          item.status === "publish" &&
          item.isRecommended
        ) {
          newswiper.push(item);
        }
        // 判断后台是否撤销了首推
        if (flag && !item.isRecommended) {
          newswiper.forEach((ite, inde) => {
            if (ite.id === item.id) {
              newswiper.splice(inde, 1);
            }
          });
        }
      });
      newswiper.forEach((item: any, index: number) => {
        const flags = payload.some((it: any) => it.id === item.id);
        const flagcover = payload.some((it: any) => it.cover === item.cover);
        // 数据库里删掉数据的话仓库里也删
        if (!flags) {
          newswiper.splice(index, 1);
        }
        // 判断图片是否被编辑
        if (!flagcover) {
          newswiper.splice(index, 1);
        }
      });
      newState.swiperlist = newswiper;
      setlocalStorage("swiperlist", newState.swiperlist);
      return newState;

    case "SETLIKES":
      // 详情页喜欢
      const newlikes = [...state.swiperlist];
      newlikes.forEach(item => {
        if (item.id === payload) {
          item.isRecommended = !item.isRecommended;
          if (!item.isRecommended) {
            item.likes++;
          } else {
            item.likes--;
          }
        }
      });
      newState.swiperlist = newlikes;
      setlocalStorage("swiperlist", newState.swiperlist);
      return newState;

    // 评论
    case "ADDTALKS":
      const newaddtalks = [...state.commentlist];
      const { name, email } = JSON.parse(
        window.localStorage.getItem("emailtoken") as string
      );
      const obj = {
        id: Math.random().toString().slice(-8),
        name: name,
        content: payload.textValue,
        userAgent: "Chrome 105.0.0.0 Blink 105.0.0.0 Windows 10",
        createAt: new Date(),
        children: [],
        email: email,
        hostId: payload.id,
        html: `<p>${payload.textValue}</p>\n`,
        parentCommentId: null,
        pass: true,
        replyUserEmail: null,
        replyUserName: null,
        updateAt: new Date(),
        url: `/article/${payload.id}`,
      };
      newaddtalks.unshift(obj);
      newState.commentlist = newaddtalks;
      setlocalStorage("commentlist", newState.commentlist);
      return newState;

    // 回复
    case "ADDCOMMENT":
      const newcommentnoe = [...state.commentlist];
      const emailbox = JSON.parse(
        window.localStorage.getItem("emailtoken") as string
      );
      const name1 = emailbox.name;
      const email1 = emailbox.email;
      const obj1 = {
        id: Math.random().toString().slice(-8),
        name: name1,
        content: payload.textValue,
        userAgent: "Chrome 105.0.0.0 Blink 105.0.0.0 Windows 10",
        createAt: new Date(),
        children: [],
        email: email1,
        hostId: payload.id,
        html: `<p>${payload.textValue}</p>\n`,
        parentCommentId: null,
        pass: true,
        replyUserEmail: null,
        replyUserName: null,
        updateAt: new Date(),
        url: `/article/${payload.id}`,
      };
      newcommentnoe.forEach((item) => {
        if (item.id === payload.id) {
          item.children.unshift(obj1);
        }
        item.children.forEach((it: any) => {
          if (it.id === payload.id) {
            it.children.unshift(obj1);
          }
        });
      });
      newState.commentlist = newcommentnoe;
      setlocalStorage("commentlist", newState.commentlist);
      return newState;

    case "SETVIEWS":
      // 点击进详情页时增加阅读量
      const newviews = [...state.swiperlist];
      newviews.forEach(item => {
        if (item.id === payload.id) {
          item.views++;
          console.log(item.views);
        }
      });
      newState.swiperlist = newviews;
      setlocalStorage("swiperlist", newState.swiperlist);
      return newState;

    case "COMMENTLIST":
      console.log(payload, "payload");
      newState.commentlist = payload[0];
      console.log(newState.commentlist);

      newState.commentlist.forEach((item: any) => {
        if (item.hostId && item.hostId === payload[1]) {
          newState.copycommentlist.concat(payload[0]);
        }
      });
      console.log(newState.commentlist);
      setlocalStorage("commentlist", newState.commentlist);
      return newState;

    case "GET_DATA":
      newState.get_data = [];
      console.log(payload);

      payload.forEach((item: DATA, index: number) => {
        if (item.status === "publish") {
          newState.get_data.push(item);
        }
      });
      newState.publish_data = newState.get_data;
      console.log(newState.publish_data);

      return newState;
    case "SEARCH_DATA":
      if (payload !== "") {
        newState.search_data = newState.publish_data.filter(
          (item: DATA, index: number) => {
            return (item.title as string).includes(payload);
          }
        );
      } else {
        newState.search_data = [];
      }

      return newState;
    case "THEME_VAL":
      return newState;
    case "":
      return newState;
    default:
      return state;
  }
};
