import {
  ALLACTION,
  DATATYPE,
  KNOWTYPE,
  KNOWSHARETYPE,
  KNOWLISTTYPE,
  KNOWSHAREIDTYPA,
  KNOWDETAILRIGHT,
  SMALLDETAILCHILDREN,
  DETAILREADING,
} from "../../types/knowledge.d";
const initialState = {
  knowStoreList: [], //知识小册总数据
  isModalOpen: false, //弹框开关
  KnowShare_item: [], //分享弹框内数据
  Know_detail_right_list: [], //右侧总数据  通过筛选后的数据
  detail_right_detail_list: [], //用于筛选数据
};

export default (state: any = initialState, { type, payload }: ALLACTION) => {
  switch (type) {
    case KNOWTYPE:
      let data = payload as KNOWLISTTYPE[];
      let knowlist = data.filter((item: KNOWLISTTYPE) => {
        return (item.status as string) === "publish";
      });

      return {
        ...state,
        knowStoreList: knowlist,
      };
    case KNOWSHARETYPE:
      let flag = payload;
      return {
        ...state,
        isModalOpen: flag,
      };

    case KNOWSHAREIDTYPA:
      let share_data = state.knowStoreList;
      //通过filter筛选右侧数据
      let filter_share_list = share_data.filter((item: { id: string }) => {
        return item.id !== (payload as KNOWLISTTYPE).id;
      });
      return {
        ...state,
        KnowShare_item: payload,
        detail_right_detail_list: payload,
        Know_detail_right_list: filter_share_list,
      };

    case KNOWDETAILRIGHT:
      return {
        ...state,
        detail_right_detail_list: payload,
      };

    case SMALLDETAILCHILDREN:
      return {
        ...state,
        small_detail_children: payload,
      };

    case DETAILREADING:
      // let readingList=state.small_detail_children
      // let filterReading=readingList.filter((item:any,index:number)=>{
      //   return item.id==payload
      // })
      return {
        ...state,
        detail_children_reading: payload,
      };

    default:
      return state;
  }
};
