const { createProxyMiddleware } = require("http-proxy-middleware");

module.exports = function (app) {
  app.use(
    "/api",
    createProxyMiddleware({
      target: "http://localhost:9000", //转发到的域名或者ip地址
      changeOrigin: true, //必须设置为true
      pathRewrite: {
        "^/api": "", //接口地址里没有"/api",将其重写置空
      },
    })
  );
};
