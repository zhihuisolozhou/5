import React, { useState, useEffect, useRef } from "react";
import { Button, Modal, Form } from "antd";
import { Col, Row, message, Pagination } from "antd";
import { BackTop, Affix } from "antd";
import { Image } from "antd";
import { useNavigate, useLocation } from "react-router-dom";
import { Anchor, Badge, Divider, Tag } from "antd";
import {
  HeartFilled,
  WechatFilled,
  ShareAltOutlined,
  SmileFilled,
} from "@ant-design/icons";
import { WifiOutlined, GithubOutlined } from "@ant-design/icons";
import { useDispatch, useSelector } from "react-redux";
import { ALLSTATE } from "../../types/store.d";
import { DISPATCH } from "../../page/home/index/archives/type/stype.d";
import { Input } from "antd";
import RecommendToReading from "../../componetns/RecommendToReading";
import axios from "axios";
import Headers from "../../componetns/header/Headers";
import copy from "copy-to-clipboard";
import "../../markdown.less";
import "../../styles/theme.css";
import useCanvas from "../../Hooks/useCanvas";
import classname from "classnames";
import SmallHeaders from "../../componetns/header/SmallHeaders";
import moment from "moment";
import QRCode from "qrcode";
import { useWithSize } from "../../Hooks";
import Comment from "../../componetns/comment/Comment";

const { Link } = Anchor;
const { TextArea } = Input;

interface DETAILTYPE {
  cover: string;
  title: string;
  status: string;
  views: string;
  updateAt: string;
  content: string;
  html: string;
  toc: any;
  totalAmount: string;
  likes: number;
  isRecommended: boolean;
  id: string;
  publishAt: string;
  isCommentable: boolean;
  tags: any;
}
moment.defineLocale("zh-cn", {
  relativeTime: {
    future: "%s内",
    past: "%s前",
    s: "几秒",
    m: "一分钟",
    mm: "%d分钟",
    h: "1小时",
    hh: "%d小时",
    d: "1天",
    dd: "%d天",
    M: "1月",
    MM: "%d个月",
    y: "1年",
    yy: "%d年",
  },
});

//判断当前时间
const isLight = () => {
  const currentData = new Date();
  return currentData.getHours();
};
function Detail() {
  const navigate = useNavigate();
  const dispatch: DISPATCH = useDispatch();
  const location = useLocation();
  // 喜欢状态
  const [islike, setislike] = useState<boolean>(false);
  // 喜欢数量
  const [likeslength, setlikeslength] = useState<number>(0);
  // 分享弹框
  const [isModalOpen, setIsModalOpen] = useState<boolean>(false);
  // 表情弹框
  const [isEmojiOpen, setisEmojiOpen] = useState<boolean>(false);
  // 邮箱登录弹框
  const [isInputOpen, setisInputOpen] = useState<boolean>(false);
  // 展开评论
  const [isopen, setisopen] = useState<boolean>(false);
  // 评论框内容
  const [textValue, settextValue] = useState<string>("");
  // 二维码
  const [qrcodesrc, setqrcodesrc] = useState<string>("");
  // 随机颜色
  const [colorac, setcolorac] = useState<string>("");
  // 吸顶高度
  const [top, setTop] = useState(70);
  // 付费
  const [pay, setPay] = useState<string>("");
  // 海报
  const [imgs, setImgs] = useState("");
  const [pagicomment, setpagicomment] = useState([]);
  {
    /* @ts-ignore  */
  }
  const list = useSelector((state: ALLSTATE) => state.redux.swiperlist);
  const talkslist = useSelector((state: ALLSTATE) => state.redux.talkslist);
  console.log(talkslist, "talkslisttalkslist");

  useEffect(() => {
    fetch(
      `https://creationapi.shbwyz.com/api/comment/host/${id}?page=1&pageSize=16`
    )
      // fetch(`https://creationapi.shbwyz.com/api/comment/host/5ad3fe5a-a302-4c3b-a334-b0bb58628aa1?page=1&pageSize=6`)
      .then((res) => res.json())
      .then((res) => {
        console.log(res);
        dispatch({
          type: "COMMENTLIST",
          payload: [res.data[0], id],
        });
      });
  }, []);
  const commentlist = useSelector((state: ALLSTATE) => state.redux.commentlist);
  useEffect(() => {
    setpagicomment(commentlist.slice(0, 5));
    console.log(commentlist, "commentlist");
  }, [commentlist]);

  // 判断宽度
  const size = useWithSize();
  const [Isif, setIsif] = useState(false);
  let scrollTop = 0;
  let topValue = 0;
  const getScollTop = () => {
    let scrollTop = 0;
    if (document?.documentElement && document?.documentElement?.scrollTop) {
      scrollTop = document?.documentElement.scrollTop;
    } else if (document?.body) {
      scrollTop = document?.body.scrollTop;
    }
    return scrollTop;
  };
  const bindHandleScroll = () => {
    scrollTop = getScollTop();
    if (scrollTop <= topValue) {
      // console.log('向上')
      setIsif(true);
    } else {
      // console.log('向下')
      setIsif(false);
    }
    setTimeout(function () {
      topValue = scrollTop;
    }, 0);
  };

  useEffect(() => {
    window.addEventListener("scroll", bindHandleScroll);
    return () => {
      window.removeEventListener("scroll", bindHandleScroll);
    };
  }, []);

  const [datas, setdatas] = useState([]);
  useEffect(() => {
    let url = `https://creationapi.shbwyz.com/api/article/recommend`;
    axios.get(url).then((res) => {
      let das = res.data.data;
      setdatas(das);
    });
  }, []);
  useEffect(() => {
    // 喜欢状态
    list.forEach((item: any) => {
      if (item.id === id) {
        setislike(item.isRecommended);
        setlikeslength(item.likes);
      }
    });
    // 二维码
    QRCode.toDataURL(title, function (err: any, url: any) {
      setqrcodesrc(url);
    });
  }, []);
  // 解构路由传参参数
  const {
    isCommentable,
    cover,
    title,
    status,
    views,
    updateAt,
    content,
    html,
    toc,
    totalAmount,
    likes,
    isRecommended,
    id,
    publishAt,
    tags,
  } = location.state as DETAILTYPE;

  const [theme, setTheme] = useState(() => (isLight() ? "白" : "黑"));
  useEffect(() => {
    console.log(theme);
  }, [theme]);
  const setlikes = () => {
    dispatch({
      type: "SETLIKES",
      payload: id,
    });
    list.forEach((item: any) => {
      if (item.id === id) {
        setislike(item.isRecommended);
        setlikeslength(item.likes);
        setPay(item.totalAmount);
        console.log(item.totalAmount);
      }
    });
  };

  // 随机颜色
  useEffect(() => {
    const randomColor = () => `#${Math.random().toString(16).substr(2, 6)}`;
    setcolorac(randomColor());
  }, []);

  // emoji表情库
  const emojilist = [
    { id: 1, emoji: "😀" },
    { id: 2, emoji: "😂" },
    { id: 3, emoji: "🙂" },
    { id: 4, emoji: "😁" },
    { id: 5, emoji: "😊" },
    { id: 6, emoji: "👩‍🦲" },
    { id: 7, emoji: "👩" },
    { id: 8, emoji: "👨" },
    { id: 9, emoji: "👵" },
    { id: 10, emoji: "👴" },
    { id: 11, emoji: "🐖" },
    { id: 12, emoji: "🐑" },
    { id: 13, emoji: "🐭" },
    { id: 14, emoji: "🐷" },
    { id: 15, emoji: "🐓" },
    { id: 16, emoji: "🍌" },
    { id: 17, emoji: "🍉" },
    { id: 18, emoji: "🍒" },
    { id: 19, emoji: "🥕" },
    { id: 20, emoji: "🥒" },
    // { id: 21, emoji: "👊👴👊" },
    // { id: 22, emoji: "   .👊👴👊🎽👖👞👞" },
  ];
  //收藏
  //二维码
  const canvasDom = useRef(null);
  const canvasURL = useCanvas(location.state, canvasDom);
  console.log(canvasURL, "-------------------------------------------------");

  //下载海报
  const handleOk = () => {
    // 下载图片
    const a = document.createElement("a");
    a.href = canvasURL;
    a.download = title;
    a.click();
    setIsModalOpen(false);
  };

  const handleCancel = () => {
    setIsModalOpen(false);
  };

  const sharetrue = () => {
    setIsModalOpen(true);
  };

  // 表情
  const emojishowModal = () => {
    // 判断是否邮箱登录
    if (!window.localStorage.getItem("emailtoken")) {
      inputshowModal();
    } else {
      setisEmojiOpen(true);
    }
  };
  // 点击emoji
  const emojihandleOk = (item: any) => {
    setisEmojiOpen(false);
    settextValue(textValue + item);
  };

  const emojihandleCancel = () => {
    setisEmojiOpen(false);
  };

  const onChange = (e: any) => {
    settextValue(e.target.value);
  };
  // 提交评论
  const sendmessage = () => {
    dispatch({
      type: "ADDTALKS",
      payload: { textValue, id },
    });
    settextValue("");
    message.success("评论成功！");
  };

  // -
  const handleCancelAnchor = (e: any) => {
    e.preventDefault();
  };

  // input获取焦点时
  const inputfocus = () => {
    if (!window.localStorage.getItem("emailtoken")) {
      inputshowModal();
    }
  };
  // 评论登录
  const inputshowModal = () => {
    setisInputOpen(true);
  };

  const inputhandleOk = () => {
    setisInputOpen(false);
  };

  const inputhandleCancel = () => {
    setisInputOpen(false);
  };
  // 复制文本
  const handleCopyone = (value: any) => {
    copy(value);
    message.success("复制成功");
  };
  // 复制文本
  const handleCopytwo = (value: any) => {
    copy(value);
    message.success("复制成功");
  };

  const Opendown = () => {
    setisopen(!isopen);
    if (talkslist <= 0) {
      message.warning("暂无评论");
    }
  };
  //-------付款框
  const [isModalOpens, setIsModalOpens] = useState(false);

  useEffect(() => {
    setPay(totalAmount);
    // 判断是否需要付费
    if (totalAmount != null && totalAmount != "" && totalAmount != undefined) {
      setIsModalOpens(true);
    } else {
      setIsModalOpens(false);
    }
  }, []);
  console.log(pay, "852");

  // 支付宝沙箱
  const handleOks = () => {
    axios
      .get("/api/api/pay", {
        params: {
          id,
          pay,
          title,
        },
      })
      .then((res) => {
        if (res.data.code === 200) {
          window.location.href = res.data.result;
        }
      });
    //跳转支付页面
    // setIsModalOpens(false);
  };

  const handleCancels = () => {
    navigate("/home/article");
  };

  // form表单
  const onFinish = (values: any) => {
    console.log("Success---:", values);
    window.localStorage.setItem("emailtoken", JSON.stringify(values));
    setisInputOpen(false);
    message.success("邮箱登录成功");
  };

  const onFinishFailed = (errorInfo: any) => {
    console.log("Failed:", errorInfo);
  };

  console.log(window.localStorage.getItem("img"), "本地");

  const onchangesize = (page: any, pageSize: any) => {
    console.log(page, "pppppppppppppppppp");
    console.log(pageSize, "aaaaaaaaaaaaaaaaaa");
    setpagicomment(commentlist.slice((page - 1) * 5, page * pageSize));
    console.log(pagicomment, "12312312312");
  };

  return (
    <>
      <BackTop />
      <div className='apbox'>

        {
          size.width < 800 ? <SmallHeaders /> : <Headers />
        }
        {/* 确认以下收费信息 */}
        <Modal
          title="确认以下收费信息"
          open={isModalOpens}
          cancelText={"取消"}
          okText={"立即支付"}
          onOk={handleOks}
          onCancel={handleCancels}
        >
          <h2>支付金额：￥{pay}</h2>
        </Modal>

        {/* ---------------------------------top------------------------------------- */}

        <main>
          <Row>
            <Col span={3}></Col>
            <Col span={size.width < 700 ? 18 : 13}>
              <div className="main">
                <div className="detail_main">
                  {/* <div className='swipered'> */}
                  <Image
                    style={{ marginTop: "15px" }}
                    width={"97%"}
                    src={cover}
                  />
                  {/* </div> */}
                  <div
                    className="zx"
                    style={{
                      width: "100%",
                      height: "10px",
                    }}
                  ></div>
                  <div className="colorbox1"></div>
                  {/* 占位盒子 */}
                  {/* <div className='op'> <Outlet /></div> */}
                  <div className="cc">
                    <h1
                      style={{
                        fontSize: "40px",
                        fontWeight: "bold",
                        marginTop: "20px",
                      }}
                    >
                      {title}
                    </h1>
                    <p>
                      <span
                        style={{
                          marginRight: "6px",
                          fontStyle: "italic",
                          fontSize: "14px",
                          fontWeight: "bolder",
                        }}
                      >
                        {status}
                      </span>
                      <span
                        style={{
                          marginRight: "6px",
                          fontStyle: "italic",
                          fontSize: "14px",
                          fontWeight: "bolder",
                        }}
                      >
                        {updateAt}
                      </span>
                      <span
                        style={{
                          marginRight: "6px",
                          fontStyle: "italic",
                          fontSize: "14px",
                          fontWeight: "bolder",
                        }}
                      >
                        ·
                      </span>
                      <span
                        style={{
                          marginRight: "6px",
                          fontStyle: "italic",
                          fontSize: "14px",
                          fontWeight: "bolder",
                        }}
                      >
                        readings {views}
                      </span>
                    </p>
                    {/* 复制文本按钮 */}
                    <Button
                      onClick={() =>
                        handleCopyone(
                          document.getElementsByClassName("python")[0].innerHTML
                        )
                      }
                      className="copyone"
                      type="link"
                    >
                      copy
                    </Button>
                    <Button
                      onClick={() =>
                        handleCopytwo(
                          document.getElementsByClassName("HTML")[0].innerHTML
                        )
                      }
                      className="copytwo"
                      type="link"
                    >
                      copy
                    </Button>
                    {/* markdown 富文本渲染 */}
                    <div
                      className={classname("markdown", "detContent")}
                      dangerouslySetInnerHTML={{ __html: html }}
                    ></div>
                    <br />
                    <Divider />
                    <p>
                      <span style={{ fontSize: "12px" }}>
                        publishAt :{" "}
                        {moment(publishAt).format("YYYY MM DD HH:mm:ss")}
                      </span>
                      <span> | copyrightInfo : copyrightContent</span>
                    </p>
                    {
                      // 身体tag标签
                      tags &&
                      tags.map((item: any, index: number) => {
                        return (
                          <Tag key={index}>
                            <a>{item.label}</a>
                          </Tag>
                        );
                      })
                    }
                    {/* <div style={{width:'100%',}}></div> */}
                    <h2
                      style={{
                        marginTop: "20px",
                        padding: "10px",
                        background: "#e7eaee",
                      }}
                    >
                      comment
                    </h2>

                    {/* 评论 */}
                    <div id="talk">
                      {/* text框 */}
                      <TextArea
                        onClick={() => inputfocus()}
                        className="textarea"
                        value={textValue}
                        onChange={onChange}
                        rows={4}
                        placeholder="commentNamespace.replyPlaceholder"
                        maxLength={100}
                      />
                      <div className="publishbox" style={{ marginTop: "12px" }}>
                        {/* 表情 */}
                        <div className="smilehover" onClick={emojishowModal}>
                          <SmileFilled />
                          <span>emoji</span>
                        </div>
                        {/* 输入框内容为空的话不能提交 */}
                        {textValue.length > 0 ? (
                          <Button
                            onClick={() => sendmessage()}
                            style={{ marginLeft: "120px" }}
                            type="primary"
                            className="button"
                            size="middle"
                          >
                            commentNamespace.publish
                          </Button>
                        ) : (
                          <Button
                            disabled
                            onClick={() => sendmessage()}
                            style={{ marginLeft: "120px" }}
                            type="primary"
                            className="button"
                            size="middle"
                          >
                            commentNamespace.publish
                          </Button>
                        )}
                      </div>
                    </div>
                    <div
                      className="bud"
                      style={{ width: "100%", height: "20px" }}
                    ></div>
                    <div
                      className="zxf"
                      style={{
                        width: "100%",
                        height: "15px",
                        background: "#e7eaee",
                      }}
                    ></div>
                    {isCommentable ? (
                      <Comment
                        marleft={"2px"}
                        id={id}
                        colorac={colorac}
                        list={pagicomment}
                      ></Comment>
                    ) : (
                      ""
                    )}
                    <Pagination
                      onChange={onchangesize}
                      style={{ marginTop: "20px", paddingBottom: "20px" }}
                      defaultPageSize={5}
                      defaultCurrent={1}
                      total={commentlist.length}
                    />
                  </div>
                </div>
              </div>
            </Col>
            {size.width > 700 ? (
              <Col span={5}>
                <div className="vice" style={{ background: "#e7eaee" }}>
                  {/* 吸顶 */}
                  <Affix offsetTop={top}>
                    <div>
                      <div>
                        <RecommendToReading str={"recommend ToReading"}>
                          {datas.length
                            ? datas.map((item: any, index: number) => {
                              return (
                                <li
                                  style={{ cursor: "pointer" }}
                                  key={index}
                                  onClick={() => {
                                    navigate("/detail/" + item.id, {
                                      state: item,
                                    });
                                  }}
                                >
                                  <span className="datas_span">
                                    {item.title}
                                  </span>
                                  <span>&emsp;</span>
                                  <span
                                    style={{
                                      color: "#ccc",
                                      fontSize: "10px",
                                    }}
                                  >
                                    {moment(item.createAt).fromNow()}
                                  </span>
                                </li>
                              );
                            })
                            : ""}
                        </RecommendToReading>
                      </div>

                      <div>
                        {/* 楼层  ！！！需要阻止默认行为 */}
                        <Anchor
                          affix={false}
                          style={{ marginTop: "15px", marginLeft: "-40px" }}
                          onClick={handleCancelAnchor}
                          showInkInFixed={true}
                        >
                          <h2>Toc</h2>
                          {
                            // 楼层内容渲染
                            JSON.parse(toc).map((item: any, index: any) => {
                              // href跳到window里名叫id的盒子
                              return (
                                <Link
                                  className={"level" + item.level}
                                  key={index}
                                  href={"#" + item.id}
                                  title={item.text}
                                />
                              );
                            })
                          }
                        </Anchor>
                      </div>
                    </div>
                  </Affix>
                </div>
              </Col>
            ) : null}
            <Col span={3}></Col>
          </Row>
        </main>
        <footer>
          <div className="footerdiv">
            {/* 底部icon */}
            <WifiOutlined style={{ color: "#fff", fontSize: "30px" }} />
            <GithubOutlined style={{ color: "#fff", fontSize: "30px" }} />
          </div>
        </footer>
      </div>
      {/* 左侧悬浮栏 */}
      {size.width < 1000 ? (
        <div
          className="iconfooter"
          style={{
            background: "#fff",
            width: "100%",
            height: "51px",
            position: "fixed",
            left: "0",
            bottom: !Isif ? "0" : "-55px",
            transition: "bottom 0.2s",
            zIndex: "999",
          }}
        >
          <div className="footerheartbox" onClick={() => setlikes()}>
            <Badge showZero={true} count={likeslength}>
              {!islike ? (
                <HeartFilled style={{ color: "#FF0064" }} className="heart" />
              ) : (
                <HeartFilled
                  style={{ color: "rgba(0,0,0,0.35)" }}
                  className="heart"
                />
              )}
            </Badge>
          </div>
          <div className="footerwechatbox">
            <Anchor
              affix={false}
              className="wechaths"
              bounds={0}
              onClick={handleCancelAnchor}
            >
              <Link
                className="wechatbox"
                href="#talk"
                title={
                  <WechatFilled
                    style={{ color: "rgba(0,0,0,0.35)" }}
                    className="wechat"
                  />
                }
              />
            </Anchor>
          </div>
          <div className="footersharebox">
            <ShareAltOutlined
              style={{ color: "rgba(0,0,0,0.35)" }}
              onClick={() => sharetrue()}
              className="shareicon"
            />
          </div>
        </div>
      ) : (
        <div className="icondiv">
          {/* 喜欢 */}
          <div className="heartbox" onClick={() => setlikes()}>
            <Badge showZero={true} count={likeslength}>
              {!islike ? (
                <HeartFilled style={{ color: "#FF0064" }} className="heart" />
              ) : (
                <HeartFilled
                  style={{ color: "rgba(0,0,0,0.35)" }}
                  className="heart"
                />
              )}
            </Badge>
          </div>
          {/* 评论/跳评论 */}
          <Anchor
            affix={false}
            className="wechaths"
            bounds={0}
            onClick={handleCancelAnchor}
          >
            <Link
              className="wechatbox"
              href="#talk"
              title={
                <WechatFilled
                  style={{ color: "rgba(0,0,0,0.35)" }}
                  className="wechat"
                />
              }
            />
          </Anchor>
          {/* 分享 */}
          <div className="sharebox">
            <ShareAltOutlined
              style={{ color: "rgba(0,0,0,0.35)" }}
              onClick={() => sharetrue()}
              className="shareicon"
            />
          </div>
        </div>
      )}
      {/* 评论登录 */}
      <div>
        <Modal
          className="talklogin"
          cancelText={"-"}
          okText={"-"}
          okType={"text"}
          title="Basic Modal"
          open={isInputOpen}
          onOk={inputhandleOk}
          onCancel={inputhandleCancel}
        >
          <Form
            name="basic"
            labelCol={{ span: 8 }}
            wrapperCol={{ span: 16 }}
            initialValues={{ remember: true }}
            onFinish={onFinish}
            onFinishFailed={onFinishFailed}
            autoComplete="off"
          >
            <Form.Item
              label="Name"
              name="name"
              rules={[{ required: true, message: "Please input your Name!" }]}
            >
              <Input />
            </Form.Item>

            <Form.Item
              label="Email"
              name="email"
              rules={[
                { type: "email", message: "The input is not valid E-mail!" },
                { required: true, message: "Please input your Email!" },
              ]}
            >
              <Input />
            </Form.Item>

            <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
              <Button type="primary" htmlType="submit">
                Submit
              </Button>
            </Form.Item>
          </Form>
        </Modal>
      </div>
      <canvas style={{ display: "none" }} ref={canvasDom}>
        二维码
      </canvas>
      {/* 分享 */}
      <>
        <Modal
          okText={"下载"}
          cancelText={"关闭"}
          style={{ top: "10px" }}
          title="分享海报"
          open={isModalOpen}
          onOk={handleOk}
          onCancel={handleCancel}
        >
          <div className="shareflex">
            <img
              style={{ width: "375px", height: "375px" }}
              src={cover}
              alt=""
            />
            <h3 style={{ fontWeight: "bold" }}>{title}</h3>
            <div className="text-title"></div>
            {/* <img src={canvasURL} alt="" /> */}

            <div className="qrcodes">
              <div>
                <img src={window.localStorage.getItem("img") as any} alt="" />
                <div className="text-right">
                  <span>识别二维码查看文章</span>
                  <span>
                    原文分享自<span style={{ color: "#FF0064" }}>ikun</span>
                  </span>
                </div>
              </div>
            </div>
          </div>
        </Modal>
      </>
      {/* <Share str={id}></Share> */}
      {/* 表情 */}
      <>
        <Modal
          style={{ top: "400px", left: "100px" }}
          title="emojiicon"
          open={isEmojiOpen}
          onOk={() => emojihandleOk("")}
          onCancel={emojihandleCancel}
        >
          <Row gutter={0}>
            {emojilist &&
              emojilist.map((item, index) => {
                return (
                  <Col
                    style={{ margin: "6px", background: "none" }}
                    key={index}
                    span={2}
                    onClick={() => emojihandleOk(item.emoji)}
                  >
                    <div style={{ cursor: "pointer" }}>{item.emoji}</div>
                  </Col>
                );
              })}
          </Row>
        </Modal>
      </>
      <BackTop />
    </>
  );
}
export default Detail;
