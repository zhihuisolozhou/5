import React, { useState, useEffect } from "react";
import Swipered from "../../../../componetns/Swipered";
import { NavLink, useNavigate } from "react-router-dom";
import { Empty, Divider } from "antd";
import "./component/Article.css";
import ArticleChile from "./component/ArticleChile";
import {
  HeartOutlined,
  EyeOutlined,
  ShareAltOutlined,
} from "@ant-design/icons";
import { Article_setdatas, Article_setdataa } from "../../../category/aps";
function Article() {
  const Navgite = useNavigate();
  const [datas, setdatas] = useState([]);
  const [dataa, setdataa] = useState([]);
  const liaos = (item: any, e: any) => {
    console.log("e");
    Navgite(`/detail/${item.id}`, {
      state: item,
    });
    // if(e.className=='colorbox3_span'){

    // }
  };
  var moment = require("moment");
  moment.defineLocale("zh-cn", {
    relativeTime: {
      future: "%s内",
      past: "%s前",
      s: "几秒",
      m: "一分钟",
      mm: "%d分钟",
      h: "1小时",
      hh: "%d小时",
      d: "1天",
      dd: "%d天",
      M: "1月",
      MM: "%d个月",
      y: "1年",
      yy: "%d年",
    },
  });

  const DAtas = async () => {
    let das = await Article_setdatas();
    setdatas(das.data.data);
  };
  const Dataa = async () => {
    let das = await Article_setdataa();
    setdataa(das.data.data);
  };
  useEffect(() => {
    DAtas();
    Dataa();
  }, []);
  const [fage, setfage] = useState(false);
  const [archives_chil, setarchives_chil] = useState();
  const showModal = (item: any, e: any) => {
    e.stopPropagation();
    setfage(true);
    setarchives_chil(item);
  };
  const show = (show: any) => {
    setfage(show);
  };
  return (
    <>
      <div className="swipered" style={{ marginBottom: "6px" }}>
        <Swipered></Swipered>
      </div>
      <div className="main_emsp">
        &emsp;
        {fage ? (
          <ArticleChile
            archives_chil={archives_chil}
            show={show}
          ></ArticleChile>
        ) : null}
        <div></div>
      </div>
      <div className="colorbox2" style={{ marginTop: "10px" }}>
        <span
          onClick={() => {
            Navgite("/home/article");
          }}
          style={{ color: "red" }}
        >
          All
        </span>
        {datas.length ? (
          datas.map((item: any, index: number) => {
            return (
              <span key={index}>
                {/* <NavLink to={`/home/article/category/${item.value}`}>{item.label}</NavLink> */}
                <span
                  onClick={() => {
                    Navgite(`/home/article/category/${item.label}`, {
                      state: item,
                    });
                  }}
                  style={{ fontSize: "13px" }}
                >
                  {item.label}&ensp;
                </span>
              </span>
            );
          })
        ) : (
          <Empty
            style={{
              width: "100%",
              height: "100%",
            }}
          />
        )}
      </div>

      <div className="colorbox3">
        {dataa &&
          dataa.map((item: any, index: number) => {
            return (
              <div
                key={index}
                className="liaoa"
                onClick={e => liaos(item, e.target)}
              >
                <p>
                  <b>{item.title}</b> &ensp;
                  <span>|&ensp;{moment(item.createAt).fromNow()}</span> &emsp;
                  <span>{item.tags.length ? item.tags[0].label : null}</span>
                </p>
                <dl>
                  <dt className={item.cover ? "Dts" : "Dtso"}>
                    {item.cover ? (
                      <img className="Imgs" src={item.cover} alt="" />
                    ) : (
                      <span></span>
                    )}
                  </dt>
                  <dd className={item.cover ? "onlyDD" : "onlyDDchi"}>
                    {item.summary ? (
                      <div className="liaoa_dd">{item.summary}</div>
                    ) : (
                      <div className="liaoa_dd"></div>
                    )}

                    <div className="iconfoucs">
                      <span>
                        <span style={{ marginTop: "1px" }}>
                          {/* @ts-ignore */}
                          <HeartOutlined style={{ fontSize: "16px" }} />
                        </span>
                        <span className="know_span_svg">
                          &ensp;{item.likes}&ensp;
                        </span>
                      </span>
                      <span style={{ color: "#8590a6" }}>&ensp;·&ensp;</span>
                      <span>
                        <span>
                          {/* @ts-ignore */}
                          <EyeOutlined style={{ fontSize: "16px" }} />
                        </span>
                        <span className="know_span_svg">
                          &ensp;{item.views}&ensp;
                        </span>
                      </span>
                      <span style={{ color: "#8590a6" }}>&ensp;·&ensp;</span>
                      <span
                        className="colorbox3_span"
                        onClick={e => showModal(item, e)}
                      >
                        <span style={{ verticalAlign: "-0.125em" }}>
                          {/* @ts-ignore */}
                          <svg
                            viewBox="64 64 896 896"
                            focusable="false"
                            data-icon="share-alt"
                            width="0.8rem"
                            height="0.8rem"
                            fill="currentColor"
                            aria-hidden="true"
                          >
                            <path d="M752 664c-28.5 0-54.8 10-75.4 26.7L469.4 540.8a160.68 160.68 0 000-57.6l207.2-149.9C697.2 350 723.5 360 752 360c66.2 0 120-53.8 120-120s-53.8-120-120-120-120 53.8-120 120c0 11.6 1.6 22.7 4.7 33.3L439.9 415.8C410.7 377.1 364.3 352 312 352c-88.4 0-160 71.6-160 160s71.6 160 160 160c52.3 0 98.7-25.1 127.9-63.8l196.8 142.5c-3.1 10.6-4.7 21.8-4.7 33.3 0 66.2 53.8 120 120 120s120-53.8 120-120-53.8-120-120-120zm0-476c28.7 0 52 23.3 52 52s-23.3 52-52 52-52-23.3-52-52 23.3-52 52-52zM312 600c-48.5 0-88-39.5-88-88s39.5-88 88-88 88 39.5 88 88-39.5 88-88 88zm440 236c-28.7 0-52-23.3-52-52s23.3-52 52-52 52 23.3 52 52-23.3 52-52 52z"></path>
                          </svg>
                        </span>
                        <span
                          className="know_span_svg"
                          style={{ marginTop: "1px" }}
                        >
                          &ensp;share&ensp;
                        </span>
                      </span>
                    </div>
                  </dd>
                </dl>
              </div>
            );
          })}
      </div>
    </>
  );
}
export default Article;
