import React, { useState, useEffect, useRef } from 'react'
import { Button } from 'antd'
import './ArticleChile.css'
import QRCode from "qrcode"
import { useNavigate } from "react-router-dom"
import { useCanvas } from './ArticleCanvas'
interface MYprops {
  archives_chil: any
  show: any
}
function ArticleChile(props: MYprops) {
  const [qrcodesrc, setqrcodesrc] = useState<string>('');
  let { archives_chil } = props
  const navigate = useNavigate()
  const [canusr, setcanusr] = useState({ archives_chil })
  const isfli = () => {
    props.show(false)
  }
  const canvasDom = useRef(null)
  const canvasUrl = useCanvas(canusr.archives_chil, canvasDom)
  useEffect(() => {
    // 二维码
    QRCode.toDataURL(archives_chil.title, function (err: any, url: any) {
      setqrcodesrc(url)
      localStorage.setItem('qrcodesrc', url)
    })
  }, [])

  return (
    <div id='ArticleChile'>
      <div className='ArticleChile_dic'>
        <div>
          <canvas ref={canvasDom} style={{ display: 'none' }}>你的浏览器不支持canvas</canvas>
          <h3><span>分享海报</span> <span onClick={() => isfli()}>X</span></h3>
          {/* <p><img src={canvasUrl} alt="" />2</p> */}
          <div className='ArticleChile_dic_div'>
            {
              canusr.archives_chil.cover ? <p className='ArticleChile_dic_div_img'><img src={canusr.archives_chil.cover} alt="" /></p> : ""
            }
            <h3 style={{ marginTop: "20px" }}><b>{canusr.archives_chil.title}</b></h3>
            <div className='ArticleChile_dic_qrcodesrc'>
              <p><img src={qrcodesrc} alt="" /></p>
              <p className='ArticleChile_dic_qrcodesrc_sapn'>
                <span>识别二维码查看文章</span>
                <span>原文分享来自&ensp;<span style={{ color: 'red' }} onClick={() => {
                  navigate(`/detail/${archives_chil.id}`, {
                    state: archives_chil
                  })
                }
                }>ikun</span></span>
              </p>
            </div>
          </div>
          <p className='ArticleChile_dic_div_p'><Button onClick={() => isfli()}>关闭</Button> &emsp;

            <Button style={{ background: "red", color: "#fff" }}><a href={canvasUrl} download="canvas.png">下载</a></Button>
          </p>
        </div>

      </div>
    </div>
    //
    // <div>
    // <h3><span>shareNamespace.title</span> <span onClick={()=>isfli()}>X</span> </h3>
    // <div className='ArticleChile_dic_div'>

    // <p>{archives_chil.title}</p>
    // <p>{archives_chil.summary}</p>
    // <p className='ArticleChile_dic_div_pp'> 
    //           <img src={qrcodesrc} alt="" />
    //           <dl>
    //             <dd>
    //             <span>shareNamespace.qrcode</span>
    //             </dd>
    //             <dd></dd>
    //             <dd></dd>
    //             <dd>

    //       <p>shareNamespace.shareFrom <a onClick={() => navigate(`/detail/${archives_chil.label}`, { state: archives_chil })}>{archives_chil.title}</a></p>
    //             </dd>
    //             </dl></p>
    // </div>
    // <p className='ArticleChile_dic_div_p'><Button onClick={()=>isfli()}>关闭</Button> &emsp;
    // <Button style={{background:"red",color:"#fff"}}>下载</Button></p>
    // </div>
    //
  )
}

export default ArticleChile
