import { useEffect, useState } from "react";
import QRCode from "qrcode"
const getImg = (url: any) => {

    return new Promise((resolve, reject) => {
        //  实例化image
        const img = new Image();
        img.crossOrigin = 'anonymous'; // 允许跨域

        img.onload = () => {
            resolve(img);
        };
        img.onerror = (error) => {
            reject(error);
        };
        img.src = url;
    });
};
const drawurl = async (ctx: any, canusr: any, canvasDom: any) => {
    //画布颜色
    ctx.fillStyle = '#fff';
    //画布大小
    ctx.fillRect(0, 0, 400, 565)
    //标题
    ctx.font = '20px hotpink'
    ctx.fillStyle = '#000'
    ctx.fillText(canusr.title, 10, 30)
    //线
    ctx.beginPath()
    ctx.fillStyle = '#ccc'
    ctx.moveTo(0, 40)
    ctx.lineTo(400, 40)


    //
    ctx.font = '14px hotpink'
    ctx.fillStyle = '#000'
    ctx.fillText('识别二维码看文章', 155, 320)
    //

    //
    ctx.font = '14px hotpink'
    ctx.fillStyle = '#000'
    ctx.fillText('原文分享自 ikun', 155, 410)
    //
    ctx.stroke()
    //绘制图片
    const img = await getImg(canusr.cover)
    ctx.drawImage(img, 5, 60, 390, 200)
    //二维码
    const qrcodesrc = localStorage.getItem('qrcodesrc')
    const ict = await getImg(qrcodesrc)
    ctx.drawImage(ict, 5, 280, 150, 150)
    const imgUrl = canvasDom.current.toDataURL();
    return imgUrl;
}
const careadtcanvas = async (canusr: any, canvasDom: any) => {
    canvasDom.current.width = 400;
    canvasDom.current.height = 565
    const ctx = canvasDom.current.getContext("2d");
    let imgURl = await drawurl(ctx, canusr, canvasDom)
    return imgURl
}
export const useCanvas = (canusr: any, canvasDom: any) => {
    console.log(canusr, 'img');
    const [URL, setUrl] = useState('')
    useEffect(() => {
        (async () => {
            const url: any = await careadtcanvas(canusr, canvasDom)
            setUrl(url)
        })()
    }, [canusr])
    return URL
}
