import React, { useState, useEffect } from 'react'
import { Col, Row } from 'antd';
import { Menu } from 'antd';
import { WifiOutlined, GithubOutlined } from '@ant-design/icons';
import { second_level_router } from "../../../../router/Routerconfig";
import { Outlet, useNavigate, NavLink } from 'react-router-dom';
import '../../../../componetns/components/Appbox.css'
import axios from 'axios';
import Headers from "../../../../componetns/header/Headers";
import { useDispatch, useSelector } from "react-redux";
import style from "../../../../styles/style.module.css";
import { BackTop } from 'antd';
import { DATATYPE, } from "../../../../types/knowledge.d"
import { Breadcrumb, Button } from 'antd';
import Know_share from "./Know_share"
import "./css/reading.css"
var moment = require('moment');
const menu = (
  <Menu
    items={[
      {
        key: '1',
        label: (
          <div>中文</div>
        ),
      },
      {
        key: '2',
        label: (
          <div>英文</div>
        ),
      },
    ]}
  />
);
const item = second_level_router.map((item, index) => {
  const key = index + 1;
  const nav = item.title ? <NavLink to={item.path}>{item.title}</NavLink> : ""
  return {
    key,
    label: nav,
  };
})
console.log(item, 'item');

function Appbox() {
  const navigate = useNavigate()
  const dispatch = useDispatch()
  const [datas, setdatas] = useState([])
  useEffect(() => {
    let url = `https://creationapi.shbwyz.com/api/article/recommend`
    axios.get(url).then(res => {
      let das = res.data.data
      setdatas(das)
    })
  }, [])
  const [datase, setdatase] = useState([])
  useEffect(() => {
    let url = `https://creationapi.shbwyz.com/api/category`
    axios.get(url).then(res => {
      let das = res.data.data
      setdatase(das)
    })
  }, [])
  console.log(datase, 123);
  var moment = require('moment');
  moment.defineLocale('zh-cn', {
    relativeTime: {
      future: "%s内",
      past: "%s前",
      s: '几秒',
      m: "一分钟",
      mm: "%d分钟",
      h: "1小时",
      hh: "%d小时",
      d: "1天",
      dd: "%d天",
      M: "1月",
      MM: "%d个月",
      y: "1年",
      yy: "%d年"
    }
  })

  const detail_item = useSelector((state: DATATYPE) => {
    //@ts-ignore
    return state.knowledge.KnowShare_item
  })
  console.log(detail_item, "1111111111")

  const detail_children_reading = useSelector((state: DATATYPE) => {
    //@ts-ignore
    return state.knowledge.detail_children_reading
  })

  const small_detail_children = useSelector((state: DATATYPE) => {
    //@ts-ignore
    return state.knowledge.small_detail_children
  })





  return (

    <div className={style.note}>
      <div className='apbox'>
        <BackTop />
        <Headers />
        <main>
          <Row>
            <Col span={3}></Col>
            <Col span={13}>
              <div className='main'>
                {/* 占位盒子 */}
                <div children className='sup'></div>
                <div className="op">
                  <div className='detail_box'>
                    <div className="topNav">
                      <Breadcrumb>
                        <Breadcrumb.Item>
                          <NavLink to="/home/KnowledgeBooks">KnowledgeBooks</NavLink>
                        </Breadcrumb.Item>
                        <Breadcrumb.Item>
                          <NavLink to={`/home/KnowledgeBooks/${detail_item.id}`}>{detail_item.title}</NavLink>
                        </Breadcrumb.Item>
                        <Breadcrumb.Item>{detail_children_reading.title}</Breadcrumb.Item>
                      </Breadcrumb>
                    </div>
                    <div className="reading_main">
                      <div className="reading_top">
                        <h1>{detail_children_reading.title}</h1>
                        {/* moment(detail_children_reading.publishAt).format("MMMM Do YYYY, h:mm:ss") */}
                        <p>publishAt{moment(detail_children_reading.publishAt).format('YYYY-MM-DD HH:mm:ss')}&ensp;·&ensp;reading{detail_children_reading.views}</p>
                      </div>
                      <div className="markdown">
                        {/* 相当于html */}
                        <p dangerouslySetInnerHTML={{ __html: detail_children_reading.html }}></p>
                      </div>
                      <div className="publish_p">
                        <p>发布于{moment(detail_children_reading.publishAt).format('YYYY-MM-DD HH:mm:ss')}</p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </Col>
            <Col span={5}>
              <div className='vice'>
                {/* 中间右上组件 */}
                {/* <RecommendToReading str={"其它阅读"}> */}
                <div className="reading_right">
                  <p>
                    {detail_item.title}
                  </p>
                  <ul>
                    {
                      small_detail_children.map((item: any, index: number) => {
                        return <li key={index}>
                          {item.title}
                        </li>
                      })
                    }
                  </ul>
                </div>

                {/* </RecommendToReading> */}

              </div>
            </Col>
            <Col span={3}></Col>
          </Row>
        </main>
        <>
          <BackTop />


        </>
        <footer>
          <div className='footerdiv'>
            <WifiOutlined style={{ color: '#fff', fontSize: '30px' }} />
            <GithubOutlined style={{ color: '#fff', fontSize: '30px' }} />
          </div>
        </footer>
      </div>
      {/* 分享遮罩层 */}
      <div>
        <Know_share></Know_share>
      </div>
    </div>


  )

}

{/* 搜索 */ }


export default Appbox
