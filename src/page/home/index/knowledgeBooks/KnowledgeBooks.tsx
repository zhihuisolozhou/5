import React, { useEffect, useState } from 'react'
import request from "../../../../utils/request"
import { useDispatch, useSelector } from "react-redux"
import * as app from "../../../../action/knowledge_api";
import { DATATYPE,DISPATCH, KNOWTYPE, KNOWLISTTYPE, KNOWSHARETYPE, KNOWSHAREIDTYPA, KNOWDETAILRIGHT } from "../../../../types/knowledge.d"
import "./css/knowledge.scss"
import { useNavigate } from "react-router-dom"
import Know_share from './Know_share'
const moment = require('moment');
moment.defineLocale('zh-cn', {
    relativeTime: {
        future: '%s内',
        past: '%s前',
        s: '几秒',
        m: '1分钟',
        mm: '%d分钟',
        h: '1小时',
        hh: '%d小时',
        d: '1天',
        dd: '%d天',
        M: '1月',
        MM: '%d月',
        y: '1年',
        yy: '%d年'
    },
})


function KnowledgeBooks() {

    // const dispatch = useDispatch()
    const dispatch: DISPATCH = useDispatch()

    const navigate = useNavigate()

    useEffect(() => {
        //获取知识小册总数据
        dispatch(app.get_knowledge_data())
    }, [])

    //总仓库内取出数据
    const list = useSelector((state: DATATYPE) => {
        //@ts-ignore
        return state.knowledge.knowStoreList
    })

    const showModal = (item: KNOWLISTTYPE, e: any) => {
        //阻止冒泡
        e.stopPropagation()
        // setIsModalOpen(true);
        dispatch({
            type: KNOWSHARETYPE,
            payload: true
        })
        dispatch({
            type: KNOWSHAREIDTYPA,
            payload: item
        })
    };

    const toDetail = (item: KNOWLISTTYPE) => {
        navigate(`/home/knowledgeBooks/${item.id}`)
        dispatch({
            type: KNOWSHAREIDTYPA,
            payload: item
        })
    }

    return (
        <div>
            <div className="knowledge_box">
                {
                    list && list.map((item: KNOWLISTTYPE, index: number) => {
                        return <div className="box_li" key={index} onClick={() => toDetail(item)}>
                            <div className="top_title">
                                <span className="top_span1">{item.title}&ensp;</span>
                                <span className="top_span2">|&ensp;</span>
                                <span className="top_time">{moment(item.createAt).fromNow()}</span>
                            </div>
                            <div className="know_main">
                                <div>
                                    {
                                        item.cover ? <div className="know_left"><img src={item.cover} alt="" /></div> : ''
                                    }
                                </div>
                                <div className="know_right">
                                    <p>{item.summary}</p>
                                    <p>
                                        <span>
                                            <span>
                                                {/* @ts-ignore */}
                                                <svg t="1663299295299" className="icon" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="2416" width="14" height="14"><path d="M515.2 224c-307.2 0-492.8 313.6-492.8 313.6s214.4 304 492.8 304 492.8-304 492.8-304S822.4 224 515.2 224zM832 652.8c-102.4 86.4-211.2 140.8-320 140.8s-217.6-51.2-320-140.8c-35.2-32-70.4-64-99.2-99.2-6.4-6.4-9.6-12.8-16-19.2 3.2-6.4 9.6-12.8 12.8-19.2 25.6-35.2 57.6-70.4 92.8-102.4 99.2-89.6 208-144 329.6-144s230.4 54.4 329.6 144c35.2 32 64 67.2 92.8 102.4 3.2 6.4 9.6 12.8 12.8 19.2-3.2 6.4-9.6 12.8-16 19.2C902.4 585.6 870.4 620.8 832 652.8z" p-id="2417" fill="#bfbfbf"></path><path d="M512 345.6c-96 0-169.6 76.8-169.6 169.6 0 96 76.8 169.6 169.6 169.6 96 0 169.6-76.8 169.6-169.6C681.6 422.4 604.8 345.6 512 345.6zM512 640c-67.2 0-121.6-54.4-121.6-121.6 0-67.2 54.4-121.6 121.6-121.6 67.2 0 121.6 54.4 121.6 121.6C633.6 582.4 579.2 640 512 640z" p-id="2418" fill="#bfbfbf"></path></svg>
                                            </span>
                                            <span className="know_span_svg">
                                                {item.views}
                                            </span>
                                        </span>
                                        <span style={{ color: '#8590a6' }}>
                                            &ensp;·&ensp;
                                        </span>
                                        <span onClick={(e) => showModal(item, e)}>
                                            <span>
                                                {/* @ts-ignore */}
                                                <svg t="1663246548343" className="icon" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="4150" width="14" height="14"><path d="M763.84 896c-47.128 0-85.333-38.205-85.333-85.333s38.205-85.333 85.333-85.333c47.128 0 85.333 38.205 85.333 85.333 0 47.128-38.205 85.333-85.333 85.333M329.92 558.848c-14.895 26.231-42.641 43.638-74.453 43.638-47.128 0-85.333-38.205-85.333-85.333 0-16.097 4.457-31.152 12.204-44 14.935-24.769 42.098-41.333 73.13-41.333 47.128 0 85.333 38.205 85.333 85.333 0 15.317-4.035 29.691-11.101 42.117M763.84 128c47.128 0 85.333 38.205 85.333 85.333s-38.205 85.333-85.333 85.333c-47.128 0-85.333-38.205-85.333-85.333 0-47.128 38.205-85.333 85.333-85.333M763.84 682.667c-0.021 0-0.047 0-0.072 0-39.16 0-74.203 17.626-97.628 45.378l-289.885-167.063c4.932-13.101 7.787-28.245 7.787-44.055 0-0.105 0-0.209 0-0.314 0-0.072 0-0.177 0-0.281 0-15.81-2.855-30.953-8.077-44.942l295.544-169.566c23.265 24.363 56.001 39.509 92.275 39.509 0.020 0 0.039 0 0.059 0 70.689 0 127.997-57.308 127.997-128 0-70.692-57.308-128-128-128-70.692 0-128 57.308-128 128 0 18.965 4.224 36.907 11.627 53.099l-292.288 168.747c-23.653-28.833-59.285-47.084-99.18-47.084-70.692 0-128 57.308-128 128 0 0.188 0 0.376 0.001 0.564-0.001 0.123-0.001 0.304-0.001 0.484 0 70.692 57.308 128 128 128 39.895 0 75.526-18.251 99.001-46.86l289.373 166.752c-5.397 13.568-8.529 29.29-8.533 45.743 0 70.582 57.308 127.889 128 127.889 70.692 0 128-57.308 128-128 0-70.692-57.308-128-128-128z" fill="#8a8a8a" p-id="4151"></path></svg>
                                            </span>
                                            <span className="know_span_svg">
                                                share
                                            </span>
                                        </span>
                                    </p>
                                </div>
                            </div>
                        </div>
                    })
                }
            </div>
            {/* 分享遮罩层 */}
            <div>
                <Know_share></Know_share>
            </div>
        </div>

    )
}

export default KnowledgeBooks