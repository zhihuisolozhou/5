import React, { useState, useRef } from 'react'
import { Modal } from 'antd';
import { useDispatch, useSelector } from "react-redux"
import { DATATYPE, KNOWSHARETYPE, KNOWDETAILRIGHT } from "../../../../types/knowledge.d"
import QRCode from "qrcode.react"
import useCanvas from "../../../../Hooks/useCanvas";

function Know_share() {
  const dispatch = useDispatch();
  const isModalOpen = useSelector((state: DATATYPE) => {
    //@ts-ignore
    return state.knowledge.isModalOpen;
  });

  const KnowShare_item = useSelector((state: DATATYPE) => {
    //@ts-ignore
    return state.knowledge.detail_right_detail_list;
  });

  console.log(KnowShare_item)

  const canvasDom = useRef(null);
  const canvasURL = useCanvas(KnowShare_item, canvasDom);

  const handleOk = () => {
    // 下载图片
    const a = document.createElement("a");
    a.href = canvasURL;
    a.download = KnowShare_item.title;
    a.click();
    dispatch({
      type: KNOWSHARETYPE,
      payload: !isModalOpen
    })
  };

  const handleCancel = () => {
    dispatch({
      type: KNOWSHARETYPE,
      payload: !isModalOpen
    })
  };

  return (
    <div>
      <canvas ref={canvasDom} style={{ display: "none" }}></canvas>
      <Modal title={KnowShare_item.title} open={isModalOpen} onOk={handleOk} onCancel={handleCancel} okText="下载" cancelText="关闭">
        <div className="share_box">
          {
            KnowShare_item.cover ? <img src={KnowShare_item.cover} alt="" /> : ''
          }
          <p>{KnowShare_item.title}</p>
          <p>{KnowShare_item.summary}</p>

          <p style={{ height: "90px", width: "100%" }}>
            <p className="qr-left" style={{ float: "left", width: "80px", height: "80px" }}>
              <QRCode
                id="qrCode"
                value="https://creation.shbwyz.com/knowledge/e9778cbf-ca31-41fb-b28e-4740d323a83c"
                size={80} // 二维码的大小
                fgColor="#000000" // 二维码的颜色
              />
            </p>
            <p className="li-right" style={{ paddingLeft: "15px", float: "left", display: "flex", height: "100%", flexDirection: "column", justifyContent: "space-between" }}>
              <li>识别二维码查看文章</li>
              <li>原文分享自<span>ikun</span></li>
            </p>
          </p>
        </div>
      </Modal>
    </div>
  );
}

export default Know_share;
