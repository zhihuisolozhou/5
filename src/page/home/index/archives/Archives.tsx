import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { DISPATCH, DATA } from "./type/stype.d";
import * as app from "../../../../action/api";
import { useNavigate } from "react-router-dom";
import "./css/stype.scss";

function Archives() {
  const dispatch: DISPATCH = useDispatch();
  const navigate = useNavigate();

  useEffect(() => {
    dispatch(app.get_data());
  }, []);
  const data = useSelector((state: any) => {
    return state.redux.publish_data;
  });
  console.log(data);
  const godetail = (it: any) => {
    navigate("/detail/" + it.id, { state: it });
  };

  return (
    <div>
      <div className="archives_top">
        <h2>archives</h2>
        <h6>
          total <span>{data ? data.length : ""}</span> piece{" "}
        </h6>
      </div>
      <div className="archives_main">
        <h1>2022</h1>
        <h3>September</h3>
        <ul className="archives_ul">
          {data
            ? data.map((item: DATA, index: number) => {
                return (
                  <li
                    style={{
                      animation: `example1 ${(index + 1) * 0.1}s ease-out ${
                        (index + 2) * 0.1
                      }s backwards`,
                      cursor: "pointer",
                    }}
                    key={index}
                    onClick={() => {
                      navigate("/detail/" + item.id, { state: item });
                    }}
                  >
                    {item.publishAt
                      ? item.publishAt.slice(5, 10)
                      : (item.publishAt as any).slice(0, 10)}{" "}
                    <b style={{ fontSize: "17px" }}> {item.title}</b>
                  </li>
                );
              })
            : "No data"}
        </ul>
      </div>
    </div>
  );
}

export default Archives;
