import store from "../store/index";
export type DISPATCH = typeof store.dispatch;
export interface DATA {
  category?: any;
  content?: string;
  cover?: string;
  createAt?: string;
  html?: string;
  id?: string;
  isCommentable?: boolean;
  isPay?: boolean;
  isRecommended?: boolean;
  likes?: number;
  needPassword?: boolean;
  publishAt?: string;
  status?: string;
  summary?: any;
  tags?: any;
  title?: string;
  toc?: string;
  totalAmount?: any;
  updateAt?: string;
  views?: number;
}
