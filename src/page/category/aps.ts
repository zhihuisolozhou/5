import request from "../../utils/request"
export const Tab_article = (params: any) => request.get(`https://creationapi.shbwyz.com/api/article/category/${params}`)

export const Tab_tepl = (id:any, page = 1, pageSize = 12, status = 'publish') =>
    request({
        method: 'GET',
        url: `https://creationapi.shbwyz.com/api/article/tag/${id}`,
        params: { page, pageSize, status },
      });
//Appbox.tsx页面
export const Appbox_setistitle=()=>request.get(`https://creationapi.shbwyz.com/api/tag?articleStatus=publish`)
export const Appbox_setdatas=()=>request.get(`https://creationapi.shbwyz.com/api/article/recommend`)
export const Appbox_setdatase=()=>request.get(`https://creationapi.shbwyz.com/api/category`)
//Datachildren.tsx页面
export const Datachildren_setistitle=()=>request.get(`https://creationapi.shbwyz.com/api/category?articleStatus=publish`)
export const Datachildren_setdatas=()=>request.get(`https://creationapi.shbwyz.com/api/article/recommend`)
export const Datachildren_setdatase=()=>request.get(`https://creationapi.shbwyz.com/api/tag?articleStatus=publish`)
export const Tab_tltce = () => request.get(`https://creationapi.shbwyz.com/api/category`)
      //Article.tsx页面
      export const Article_setdatas=()=>request.get(`https://creationapi.shbwyz.com/api/category?articleStatus=publish`)
      export const Article_setdataa=()=>request.get(`https://creationapi.shbwyz.com/api/article/recommend`)
//category.tsx页面
export const category_setdatas=()=>request.get(`https://creationapi.shbwyz.com/api/article/recommend`)
export const category_setdatae=()=>request.get(`https://creationapi.shbwyz.com/api/category?articleStatus=publish`)
export const category_setdatase=()=>request.get(`https://creationapi.shbwyz.com/api/tag`)
// export const category_setdatas=()=>request.get(``)
 

