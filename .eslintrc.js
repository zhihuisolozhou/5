// module.exports = {
//   extends: ["react-app", "react-app/jest", "prettier"],
//   plugins: ["prettier"],
//   rules: {
//     "prettier/prettier": [
//       "error",
//       {
//         // singleQuote: true, //使用单引号而不是双引号
//         // prinWidth: 100, //打印宽度
//         // tabWidth: 2, //指定每个缩进级别的空格数
//         // tralingComma: "es5", //在 ES5 中有效的尾随逗号（对象、数组等）。TypeScript 中的类型参数中没有尾随逗号。
//         // endOfLine: "auto", //保持现有的行尾
//         // semi: true, //在每条语句的末尾添加一个分号
//       },
//     ],
//     "no-console": process.env.NODE_ENV === "development" ? "off" : "off",
//     "no-unsed": "error",
//     "no-debugger": process.env.NODE_ENV === "development" ? "off" : "off",
//   },
// };
